\documentclass[table]{ucbthesis}

% note that the table here allows colour to be added to the 
% table rows 

% import from other files %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{import}


% bibliography %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[backend=biber,
natbib=true,
style=authoryear,
sorting=none,
isbn=true,
doi=true,
url=true,
]{biblatex}

\makeatletter
% special thanks to this guy
\newrobustcmd*{\parentexttrack}[1]{%
  \begingroup
  \blx@blxinit
  \blx@setsfcodes
  \blx@bibopenparen#1\blx@bibcloseparen
  \endgroup}

\AtEveryCite{%
  \let\parentext=\parentexttrack%
  \let\bibopenparen=\bibopenbracket%
  \let\bibcloseparen=\bibclosebracket}

\makeatother
\addbibresource{phd_dissertation_bibliography.bib}

% todo %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{amssymb}
\usepackage{todo}

% for plotting %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% csv files and plots and tikz

\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{compat=1.18}
\usepackage{csvsimple-l3}


%% for doing \begin{figure}[H]
\usepackage{float}

%% for doing double integration
\usepackage{amsmath}

%% bold symbols in math mode
\usepackage{bm}

%% for row colours in table 
%\usepackage[table]{xcolor}

%% for tikz diagrams
\usetikzlibrary{shapes.symbols,shapes.geometric,shadows,arrows.meta,
positioning,fit,backgrounds}
\tikzset{>={Latex[width=1.5mm,length=2mm]}}
\usetikzlibrary{shapes,arrows}

% for extremely big latex files which cause the compiler to run 
% out of memory 
% so compile figures as separate files
\usepgfplotslibrary{external}
\tikzexternalize

% hebrew for acknowledgements
\usepackage{cjhebrew}

% for numberless captions 
\usepackage{caption}

% custom title page 
% taken from:

%% ucbthesis.cls 2014-04-11
%% Copyright (C) 1988-2014 Daniel Gildea, BBF, Ethan Munson, Paul Vojta.
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2003/12/01 or later.

% and then modified


\def\maketitle{
{\ssp
  \makeatletter
    \setlength{\@tempdima}{28pt}\advance\@tempdima-\baselineskip
    \parskip=\@tempdima minus 4pt
  \makeatother
\begin{alwayssingle}
  \hrule height 0pt
  % Fix things so that they're vertically centered
  % \@tempdima = 2 * ((\stockheight-\textheight) / 2 - \uppermargin) - \topskip
    \@tempdima=\stockheight
    \advance\@tempdima-\textheight
    \advance\@tempdima-2\uppermargin
    \advance\@tempdima-\topskip
    \vspace{\@tempdima}
  \vfill
  \let\footnotesize\small
  \let\footnoterule\relax
  \thispagestyle{empty}
  \setcounter{page}{1}

  \begin{center}
    \fmfont
    \textbf{\@title}\par
    \vspace{14pt minus 4pt}
    By \par
    {\@author}\par
    \vspace{28pt minus 8pt}
    A \@dissertation submitted in partial satisfaction of the \par
    requirements for the degree of \par
    \ifdefined\@jointinst
      Joint {\@degree}\\with {\@jointinst}\par
    \else
      {\@degree}\par
    \fi
    in \par
    {\@field}\par
    \ifdefined\@emphasis
      and the Designated Emphasis\par
      in\par
      {\@emphasis}\par
    \fi
    in the \par
    Graduate Division \par
    of the \par
    University of California, {\expandafter{\@campus}}\par
    \vspace{34pt minus 8pt}
    {
      \fmsmallfont
      Committee in charge: \par
      {\@chair}, \@chairtitle \\
      \ifdefined\@cochair
        {\@cochair}, Co-chair \\
      \fi
      {\@othermembers}\par
      \vspace{14pt minus 4pt}
      \@degreesemester \ \@degreeyear\par
    }
  \end{center}
  \vfill
  \hrule height 0pt
\end{alwayssingle}

\setcounter{footnote}{0}
}}

% Title %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}
\title{Digital Twins as Testbeds for Iterative Simulated Neutronics 
Feedback Controller Development}
\author{Theodore Kay Chen Ong}
\degreesemester{Spring}
\degreeyear{2024}
\degree{Doctor of Philosophy}
\chair{Professor Per F. Peterson}
\othermembers{Professor Van P. Carey \\
  Associate Professor Massimiliano Fratoni}
% For a co-chair who is subordinate to the \chair listed above
% \cochair{Professor Benedict Francis Pope}
% For two co-chairs of equal standing (do not use \chair with this one)
% \cochairs{Professor Richard Francis Sony}{Professor Benedict Francis Pope}
\numberofmembers{3}
% Previous degrees are no longer to be listed on the title page.
% \prevdegrees{B.A. (University of Northern South Dakota at Hoople) 1978 \\
%   M.S. (Ed's School of Quantum Mechanics and Muffler Repair) 1989}
\field{Engineering - Nuclear Engineering}
% Designated Emphasis -- this is optional, and rare
% \emphasis{Colloidal Telemetry}
% This is optional, and rare
% \jointinstitution{University of Western Maryland}
% This is optional (default is Berkeley)
% \campus{Berkeley}


\maketitle
\clearpage
\copyrightpage
\import{./}{abstract.tex}
\frontmatter
%checked 17 jan 2024, title first, blank, then abstract
\tableofcontents
\clearpage
\listoffigures
\clearpage
\listoftables
\import{./}{acknowledgements.tex}


\mainmatter

%%%% MAIN CONTENT
\part{Digital Twin Construction} 
\chapter{Introduction} \label{chp:intro}
\import{./}{introduction.tex}
\chapter{Literature Review and Principles for Digital Twin Construction} \label{chp:digital_twin_lit_review}
\import{./digital_twin_lit_review}{part1_dt_review.tex} 
\import{./digital_twin_lit_review}{part2_balance_eqns.tex} 
\import{./digital_twin_lit_review}{part3_heater_and_pipe_properties.tex} 
\import{./digital_twin_lit_review}{part4_explicit_vs_implicit_coupling.tex} 


\chapter{Thermal Hydraulics Library Construction} \label{chp:digital_twin_lib_construction}
\import{./thermal_hydraulics_rs_library_building}{library_construction.tex} 

\part{Use Case Demonstration for the Digital Twin: 
Simulated Neutronics Facility Test bed}

\chapter{Simulated Neutronics Feedback Construction Principles 
and Literature Review}\label{chp:snf-lit-review}
\import{./neutronics_transfer_function}{lit_review.tex}
\chapter{Multiphysics Model and Surrogate Model Construction 
and Results}\label{chp:snf-construction}
\import{./neutronics_transfer_function}{source_code_and_results.tex}
\chapter{Simulated Neutronics Feedback Controller Development}\label{chp:snf-testing}
\import{./snf_controller_dev}{transfer_fn_simulator.tex}
\import{./snf_controller_dev}{snf-testing-methods-and-results.tex}

\pagebreak
\chapter{Conclusion}\label{chp:conclusion-and-future-work}
\import{./}{conclusion.tex}

\backmatter

\pagebreak
\raggedright
\printbibliography
\pagebreak

% bibliography checked for margins 17 jan 2024

%%%%% APPENDIX

\appendix

\chapter{Appendix A: Digital Twin Construction}

\section{Coiled Tube Air Heater (CTAH) Data for Modelling and Validation}
\label{appendix:ctah_modelling_and_validation}
\import{./digital_twin_lit_review}{ctah_review.tex}

\chapter{Appendix B: Simulated Neutronics Feedback Model Construction}
\label{appendix:simulated_neutronics_feedback_model_construction}

\section{GeN-Foam Stabilisation Bash Script}
\label{appendix:gen-foam-stabilisation-bash-script}

\import{./Appendix}{gen-foam-stabilisation-bash-script.tex}

\section{GeN-Foam exmaple of fvSchemes for Tetrahedral Meshes}
\label{appendix:fvschemes-for-tetrahedral-meshes-example}

\import{./Appendix}{fvschemes-for-tetrahedral-meshes-example.tex}

\section{PRBS Sequence with Timestamps}
\label{appendix:prbs-sequence}

\import{./Appendix}{prbs-sequence.tex}

\section{The meaning of Phi $(\phi)$ in the context of GeN-Foam}
\label{appendix:gen-foam-what-does-phi-mean}

\import{./Appendix}{gen-foam-what-does-phi-mean}

%\chapter{SNF Construction}
%
%\section{Transfer Function Simulation Module in Rust}
%
%\import{./snf_controller_dev}{validation_cases.tex}
%\import{./snf_controller_dev}{undamped_and_unstable_transfer_fns.tex}


\todos{}
% checked 17 jan 2024
\end{document}
