#!/bin/bash 

echo ""
echo "latexmk_working_copy: watches the working copy tex files for changes"
echo ""

latexmk_working_copy() {
	latexmk -pvc -pdf -shell-escape --interaction=nonstopmode working_copy.tex
}

echo ""
echo "latexmk_full_dissertation: watches the Dissertation_Ted tex files for changes"
echo ""

latexmk_full_dissertation() {
	latexmk -pvc -pdf -shell-escape --interaction=nonstopmode Dissertation_Ted.tex
}

echo ""
echo "latexmk_watch: watches a latex file of your choice"
echo ""
echo "usage: latexmk_watch myfile.tex"

latexmk_watch() {
	latexmk -pvc -pdf -shell-escape --interaction=nonstopmode $1
}
