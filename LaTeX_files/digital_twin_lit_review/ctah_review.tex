The air-cooled heat exchanger in the primary loop modelled in Transform 
was meant to replicate the function of the CTAH in the FHR 
\citep{DeWet2020}. In CIET RELAP models, this air-cooled heat exchanger 
was also called the CTAH \citep{Zweibaum2015a}. The isothermal digital 
twin in previous work also follows this naming convention \citep{Ong2023}. 
These air-cooled heat exchangers were Xylem Standard Exchange 
Model 15L F700 FanEx heat exchangers \citep{Bickel2014}. 
We shall stick to 
this naming convention to avoid confusion with previous work.


Xylem Standard Exchange Model 15L F700 FanEx heat exchangers contain a 
large fan with aluminium fins and 0.5 inch outer diameter 
copper tubing to aid heat exchange
\citep{Xylem2023}. Its motor is a 115 Volt, single phase, 60 Hz motor 
\citep{Xylem2023}. Modelling this geometry with high 
fidelity would be challenging. 
Therefore, one could use simplified calibrated models to model the 
CTAH.\@ 

The CTAH in Transform was modelled as an adiabatic inlet volume, followed 
by a heater consisting of 12 parallel copper tubes, and then an adiabatic 
outlet volume. The inlet and outlet volumes were 0.001 $m^3$ 
each \citep{DeWet2020}. Heat transfer in the inner wall was assumed to be 
ideal as the F700 Model 15L uses hollow metal turbulator spheres to 
enhance heat exchange \citep{Xylem2023,DeWet2020}. One should note 
however that this idealisation works for a flowrate of 0.18 kg/s.

The heat transfer coefficient at 0.18 kg/s oil flowrate in terms of 
fan frequency is \citep{DeWet2020}:

\begin{equation}
	h = -0.2021 f^2 + 18.15 f - 43.07
\end{equation}

Of course, we can also model the CTAH to some desired temperature boundary 
condition. For the SAM model, the CTAH was set to remove heat such that 
its outlet temperature was 80$^\circ C$ \citep{Zou2019}. This is usually 
done experimentally so that the loop is able to reach steady state 
in preparation for frequency response testing \citep{DeWet2020}.
If one desires to use the frequency response data from CIET for the CTAH and the 
whole loop, one must note that in the PRBS frequency response tests for 
forced convection, the CTAH was kept to a contanst fan frequency 
\citep{DeWet2020}. The correlation for that was \citep{DeWet2020}:

\begin{equation}
	h = 0.0352 \dot{Q} - 8.7472
\end{equation}

I was not able to find units easily for both these correlations, but 
there is still some validation data for the CTAH with units. For example,
De Wet gives the correlation of the required fan frequency in Hz 
to bring Therminol-VP1 temperatures from down to $80 ^\circ C$. 
This was derived from experimental data at 0.18 kg/s where 
CTAH fan speed was plotted against heater power from 3000 W to 10000 W at 
80$^\circ C$ CTAH outlet temperature. The simplified linear correlation 
is \citep{DeWet2020}:

\begin{equation}
	f (Hz) = 0.0035823~\dot{Q}_{heater}(W) - 3.410 \pm 2.0 Hz 
\end{equation}

The other dataset we can consider is the steady state 
temperature profile around CTAH at the primary loop flowrate of 
0.18 $kg/s$ and CTAH air inlet temperature of 
$22 ^\circ C$. The fan frequency for this 
data set is set to ensure that the 
CTAH outlet temperature is $80 ^\circ C$. This data is 
presented in Table~\ref{tab:forced_circ_data_dewet_frequency}:

\begin{table}[H]
	\centering
	\rowcolors{1}{}{lightgray}
	\csvreader[tabular = | p{2.4 cm}| p{2.4 cm}| p{2.4 cm}| p{2.4 cm} |c p{2.3cm} c|,
	table head = \toprule 
	\centering CTAH Fan Frequency (Hz) & 
	\centering CTAH Therminol Inlet Temperature (BT-43) $^\circ C$ &
	\centering CTAH Therminol Outlet Temperature (BT-41) $^\circ C$ &
	\centering CTAH Air Inlet Temperature (AT-02) $^\circ C$ & 
	& 
	\centering CTAH Air Outlet Temperature (AT-01) $^\circ C$ &
	\\\midrule,
	late after line = \\\midrule,
	%table foot = \hline
	]{%
	./digital_twin_lit_review/forced_circ_data_dewet.csv}{}{%
			\centering \csvcolv & 
			\centering \csvcolvi & 
			\centering \csvcolvii & 
			\centering \csvcolviii & 
			& 
			\centering \csvcolix & }%
	\caption{CTAH Steady State Data at Various frequencies
	\citep{DeWet2020} } 
	\label{tab:forced_circ_data_dewet_frequency}
\end{table}

This dataset provides us both temperature difference and enthalpy changes 
of the air and Therminol-VP1 flowing through CTAH at different fan 
frequencies. With appropriate thermophysical data and a thermal 
resistance model, we should be able to obtain the heat transfer 
coefficient $h$ and determine the original units. The heater power, 
steady state inlet and outlet temperatures corresponding to the CTAH 
inlet and outlet temperatures is presented in 
Table~\ref{tab:forced_circ_data_dewet_fluid_inlet_outlet_temp_appendix}:

\begin{table}[H]
	\centering
	\rowcolors{1}{}{lightgray}
	\csvreader[tabular = | p{2.4cm}|p {2.4cm} | p{2.4 cm}| p{2.4 cm} |c p{2.3cm} c|,
	table head = \toprule 
	\centering Heater Power ($W$) & 
	\centering Heater Inlet Temperature (BT-11) $^\circ C$ & 
	\centering Heater Outlet Temperature (BT-12) $^\circ C$ &
	\centering CTAH Inlet Temperature (BT-43) $^\circ C$ &
	& 
	\centering CTAH Outlet Temperature (BT-41) $^\circ C$ &
	\\\midrule,
	late after line = \\\midrule,
	%table foot = \hline
	]{%
	./digital_twin_lit_review/forced_circ_data_dewet.csv}{}{%
			\centering \csvcoli & 
			\centering \csvcolii & 
			\centering \csvcoliii & 
			\centering \csvcolvi & 
			& 
			\centering \csvcolvii & }%
	\caption{Forced Circulation Steady State Temperature 
	Data \citep{DeWet2020} } 
	\label{tab:forced_circ_data_dewet_fluid_inlet_outlet_temp_appendix}
\end{table}

To model the thermal resistance within the CTAH, we also need 
the thermophysical properties of copper which make up the tubes.
For these thermophysical properties,
Copper for the CTAH tubes was taken to have a density of 8940 $kg/m^3$ 
in the SAM model \citep{Zou2019}. The $c_p$ and $k$ for copper are 
presented in the following Table~\ref{tab:thermophysical-properties-of-copper-appendix}:

\begin{table}[H]
	\centering
	\rowcolors{1}{}{lightgray}
	\csvreader[tabular = | p{4cm}|p{5cm}|c p{4cm} c|,
	table head = \toprule 
	\centering Temperature ($K$) & 
	\centering k $(W~m^{-1}~K^{-1})$ & 
	& 
	\centering $\text{c}_p~(J~kg^{-1}~K^{-1})$&
	\\\midrule,
	late after line = \\\midrule,
	%table foot = \hline
	]{%
		./thermal_hydraulics_rs_library_building/copper_material_properties_zou_2019.csv}{}{%
			\centering \csvcoli & 
			\centering \csvcolii & 
			& 
			\centering \csvcoliii & }%
	\caption{Thermophysical Properties of Copper 
	\citep{Zou2019,Zweibaum2015a} } 
	\label{tab:thermophysical-properties-of-copper-appendix}
\end{table}

This covers most of the data necessary to model CTAH. While it is not 
modelled in this current work, this modelling and validation data 
will become important in future work when heat exchangers and coolers will 
need to be properly modelled.

%checked 17 jan 2024
