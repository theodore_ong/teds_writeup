\section{A Graphical Abstract}

Time is limited. In the context of climate change, there is limited time 
to reduce carbon emissions. Whereas in the context of business and 
research, time is money. Whatever the context is, we want to use our time 
efficiently and purposefully. If our mission is to reduce carbon 
emissions quickly, nuclear energy needs to be part of the equation. 
Nevertheless, the licensing 
and construction process can hinder nuclear power's effectiveness in 
reducing future carbon emissions. 
Therefore, we want to discuss methods that may potentially save time 
in the context of reactor development. To save some of your reading time 
as well, I present Figure~\ref{fig:graphical-abstract}:

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{
	./media/graphical_abstract.png}
	\caption{Graphical Abstract}
	\label{fig:graphical-abstract}
\end{figure}


Figure~\ref{fig:graphical-abstract} summarises the key themes and content 
in this dissertation. We present two tools used in tandem to save time for 
nuclear reactor development. This is because we wish to make
nuclear energy a practical solution for climate change. To do so, a nuclear 
reactor or power station often has to be run as a business or governmental 
operation. However, nuclear reactors tend to have long construction 
and licensing processes. These have made nuclear reactors notorious for 
cost overruns. 

To reduce the time and monetary costs for licensing and developing nuclear 
reactors such as the fluoride salt cooled high temperature reactor (FHR),
the goal of this research was to develop simulated neutronics 
feedback capabilities for 
existing scale models of the FHR, otherwise known as Integral Effects 
Tests (IETs). These IETs help us understand how heat transfer would 
work in a real nuclear reactor, except that the heat source is non-nuclear.
To make the IET behave more like a nuclear reactor, it must have simulated
nuclear reactor behaviour. We call this simulated neutronics feedback (SNF). 
This process is reflected in the left column of 
Figure~\ref{fig:graphical-abstract}. Nevertheless, the COVID-19 pandemic 
and lock-down came and rendered the IETs in my research laboratory 
inaccessible. Hence, with much guidance from my supervisors, mentors 
and peers, I changed my research direction to develop code suitable 
for constructing a Digital Twin for the IET for the FHR known 
as the Compact Integral Effects Test (CIET). This was done in the hope that 
the Digital Twin of CIET, or at least some of its key components, would 
be useful for developing SNF capability for CIET. This is reflected in 
the right column of Figure~\ref{fig:graphical-abstract}. With both these 
tools, I wanted to provide means which could potentially increase the 
development speed of test FHRs and commercial FHRs and reduce its 
development costs and time frame. This is reflected in the central 
column of Figure~\ref{fig:graphical-abstract}. 

\section{Dissertation Outline}

This dissertation provides an example for how using a Digital Twin 
early in the iterative design process to create SNF Controllers can save
time. The early work and availability of a SNF Controller can expedite 
work done in nuclear reactor design and development.

To show how this was done, we cover much of the content in
Figure~\ref{fig:graphical-abstract}. Figure~\ref{fig:graphical-abstract}, of 
course, is shown in our introduction here in Chapter~\ref{chp:intro}. 
Figure~\ref{fig:graphical-abstract} covers much of content for this 
dissertation, but the fluid mechanics 
libraries were developed in my master's thesis. Therefore, that work is 
only referenced and continued upon in this dissertation. Moreover, we have 
not yet applied the new SNF capabilities in CIET, so this 
remains as future work in Figure~\ref{fig:graphical-abstract}. These are 
broad topics in themselves which require separate publications. 
Nevertheless, the work 
in this dissertation provides a foundation for future work in SNF controller 
and SNF facility development.

We start discussions with Digital Twin construction in the first part of this 
dissertation. This is because we cannot test SNF capabilities without 
an IET facility or its Digital Twin. Therefore, the construction 
methodology for Digital Twins forms one of the foundational 
elements for this dissertation. 
We review Digital Twins and previous work in 
Chapter~\ref{chp:digital_twin_lit_review}. We then discuss how previous 
work was extended by developing heat transfer libraries in the Rust 
Programming Language as shown in Figure~\ref{fig:graphical-abstract} 
in Chapter~\ref{chp:digital_twin_lib_construction}. For 
those unfamiliar with Rust, the Ferris the Crab is Rust's unofficial mascot 
recognised by the Rust programming community as of 2023. 

For this dissertation, we discuss how the electric heater of CIET, or its Digital 
Twin, is given SNF capabilities. Therefore, we review existing literature 
for SNF controllers for electrically heated IET facilities 
and how we could construct a SNF controller for CIET in 
Chapter~\ref{chp:snf-lit-review}. The SNF model is not based on Point 
Reactor Kinetics Equations (PRKE), but rather on more generic data 
driven surrogate models developed using higher fidelity multiphysics models.
To demonstrate this method, a representative arbitrary reactor multiphysics 
model was constructed using the OpenMC Monte Carlo code and the GeN-Foam reactor
multiphysics code in Chapter~\ref{chp:snf-construction}. Using this 
multiphysics model of an arbitrary reactor, we also discuss how a surrogate 
model was constructed in Chapter~\ref{chp:snf-construction}. This 
surrogate model was then used as a basis for SNF Controller development 
in Chapter~\ref{chp:snf-testing}. 

For this dissertation, we do not complete the SNF IET development process 
in Figure~\ref{fig:graphical-abstract}. While this dissertation demonstrates 
how a SNF controller could be created using a data-driven surrogate model,
further work is needed to apply the same method to more prototypical FHR 
designs. Nevertheless, the work described in 
this dissertation forms the basis for future work in SNF controller 
development. We discuss some of these future possibilities in our conclusion 
in Chapter~\ref{chp:conclusion-and-future-work}.

% Intro checked 16 jan 2024


