
\paragraph{Zeroth Moment Equations (appendix)}

Now, in deriving the diffusion equations, the first step is to obtain 
the zeroth moment equations. This is where we integrate the neutron 
transport equation

\begin{equation}
	\begin{split}
		\int_{4\pi} d\hat{\Omega} \frac{1}{v}\frac{\partial}{\partial t} 
		\psi(\vec{r},\hat{\Omega},E,t) = 
		\int_{4\pi} d\hat{\Omega}\frac{\chi(E)}{4\pi} 
		\int_0^\infty dE' \int_{4\pi} d\hat{\Omega}' \nu(E') 
		\Sigma_f (\vec{r},E') \psi(\vec{r},E', \hat{\Omega}',t)  \\
		+ \int_{4\pi} d\hat{\Omega} \int_0^\infty dE' 
		\int_{4\pi} d\hat{\Omega}' 
		\Sigma_s (\vec{r},E' \rightarrow E, 
		\hat{\Omega}' \rightarrow \hat{\Omega}) 
		\psi(\vec{r},E',\hat{\Omega}',t) \\
		+ \int_{4\pi} d\hat{\Omega} Q_{ex} (\vec{r},\hat{\Omega},E,t)  
		- \int_{4\pi} d\hat{\Omega} \hat{\Omega}\bullet 
		\nabla \psi(\vec{r},\hat{\Omega},E,t) 
		- \int_{4\pi} d\hat{\Omega} \Sigma_t \psi(\vec{r},\hat{\Omega},E,t) 
	\end{split}
\end{equation}

We note the definition for neutron current $\vec{J}$:

\begin{equation}
	\int_{4\pi} d\hat{\Omega} \hat{\Omega} 
	\psi(\vec{r},\hat{\Omega},E,t) = \vec{J}(\vec{r},E,t)
\end{equation}

After integrating and substituting $\vec{J}$ and $\phi(\vec{r},E,t)$,
our zeroth moment equation becomes:

\begin{equation}
	\begin{split}
		\frac{1}{v}\frac{\partial}{\partial t} 
		\phi(\vec{r},E,t) = 4\pi \frac{\chi(E)}{4\pi} 
		\int_0^\infty dE' \int_{4\pi} d\hat{\Omega}' 
		\nu(E') \Sigma_f (\vec{r},E') \psi(\vec{r},E', \hat{\Omega}',t) \\
		+ \int_{4\pi} d\hat{\Omega} \int_0^\infty dE' \int_{4\pi} 
		d\hat{\Omega}' \Sigma_s (\vec{r},E' \rightarrow E, 
		\hat{\Omega}' \rightarrow \hat{\Omega}) 
		\psi(\vec{r},E',\hat{\Omega}',t) \\
		+ Q_{ex} (\vec{r},E,t)  -  \nabla \bullet \vec{J}(\vec{r},E,t) 
		- \Sigma_t \phi(\vec{r},E,t) 
	\end{split}
\end{equation}



For the inscattering term, we assume that the angular dependence of the 
double differential cross section $\Sigma_s$ is only determined 
by the scattering cosine angle $\mu_0=\hat{\Omega} 
\bullet \hat{\Omega}'$ so that \citep{Duderstadt1976}:


\begin{equation}
	\Sigma_s (\vec{r},E' \rightarrow E, \hat{\Omega}' 
	\rightarrow \hat{\Omega}) = 
	\Sigma_s (\vec{r},E' \rightarrow E, \mu_0)
\end{equation}

To evaluate this integral of the inscattering term, we first take note that 
\citep{Duderstadt1976}:

\begin{equation}
	\begin{split}
		\int_{4\pi} d\hat{\Omega} \Sigma_s (\vec{r},E' \rightarrow E, 
		\hat{\Omega}' \rightarrow \hat{\Omega}) = 
		\int_{0}^{2\pi} d\varphi \int_{-1}^{1} d\mu_0 
		\Sigma_s (\vec{r},E' \rightarrow E, \hat{\Omega}' 
		\bullet \hat{\Omega}) = \Sigma_s(\vec{r},E' \rightarrow E)
	\end{split}
\end{equation}


This can be applied because once we make the double differential 
cross section's angular dependence depend only on scattering cosine 
angle, then it doesn't really matter from which solid angle the 
neutron comes from initially as long as the polar angle is the 
same. In this regard there is azimuthal symmetry. Also, 
the initial and final directions do not really matter, 
only the scattering angle between the initial and final directions. 
Thus, integrating over all $\hat{\Omega}$, one would have covered 
all possible scattering cosines $\mu_0$ because only the relative 
scattering angle matters, not the actual initial 
direction $\hat{\Omega}'$ and final direction $\hat{\Omega}$.

And since the angular flux in the inscattering term 
only depends on $\hat{\Omega}'$, we can write:

\begin{equation}
	\begin{split}
		\int_{4\pi} d\hat{\Omega} \int_0^\infty dE' 
		\int_{4\pi} d\hat{\Omega}' \Sigma_s 
		(\vec{r},E' \rightarrow E, \hat{\Omega}' 
		\rightarrow \hat{\Omega}) \psi(\vec{r},E',\hat{\Omega}',t) \\
		= \int_0^\infty dE' \int_{4\pi} d\hat{\Omega}' 
		\Sigma_s (\vec{r},E' \rightarrow E) 
		\psi(\vec{r},E',\hat{\Omega}',t) \\
		= \int_0^\infty dE' 
		\Sigma_s (\vec{r},E' \rightarrow E) \phi(\vec{r},E',t)
	\end{split}
\end{equation}

We also apply the angular integral to our fission source term, of course assuming fission cross sections do not depend on incoming angle on average.

With this, we obtain our zeroth moment equation:

\begin{equation}
	\begin{split}
		\frac{1}{v}\frac{\partial}{\partial t} 
		\phi(\vec{r},E,t) = \chi(E) 
		\int_0^\infty dE'  \nu(E') \Sigma_f (\vec{r},E') 
		\phi(\vec{r},E',t)  \\
		+ \int_0^\infty dE' \Sigma_s (\vec{r},E' \rightarrow E) 
		\phi(\vec{r},E',t) + Q_{ex} (\vec{r},E,t)  -  
		\nabla \bullet \vec{J}(\vec{r},E,t) - 
		\Sigma_t \phi(\vec{r},E,t)
	\end{split}
\end{equation}


\paragraph{First Moment Equation(appendix)}

The next step to deriving the diffusion equation is the first 
moment equation. In the first moment equation, we multiply $\hat{\Omega}$ 
in first and then perform the integration again. 
For clarity, $\hat{\Omega}$ is \citep{Duderstadt1976}:

\begin{equation}
	\hat{\Omega}= \hat{e}_x \sin \theta \cos \phi + 
	\hat{e}_y \sin \theta \sin \phi + 
	\hat{e}_z \cos \theta = \Omega_x + \Omega_y + \Omega_z
\end{equation}

This is not to be confused with $d\hat{\Omega}$ as

\begin{equation}
d\hat{\Omega}= \sin \theta d\theta d\phi = -d\mu d\phi 
\end{equation}

Therefore, multiplying $\hat{\Omega}$ into each equation 
is the same as multiplying the vector components into 
each equation and we integrate separately. We can then 
recombine each equation into what we can 
represent as a vector equation. Overall, the process looks like this:


\begin{equation}
	\begin{split}
		\int_{4\pi} d\hat{\Omega} \hat{\Omega}\frac{1}{v}
		\frac{\partial}{\partial t} \psi(\vec{r},\hat{\Omega},E,t) = 
		\int_{4\pi} d\hat{\Omega} \hat{\Omega} \frac{\chi(E)}{4\pi} 
		\int_0^\infty dE' \int_{4\pi} d\hat{\Omega}' \nu(E') 
		\Sigma_f (\vec{r},E') \psi(\vec{r},E', \hat{\Omega}',t)  \\ 
		+ \int_{4\pi} d\hat{\Omega} \hat{\Omega} 
		\int_0^\infty dE' \int_{4\pi} d\hat{\Omega}' 
		\Sigma_s (\vec{r},E' \rightarrow E, 
		\hat{\Omega}' \rightarrow \hat{\Omega}) 
		\psi(\vec{r},E',\hat{\Omega}',t)  \\
		+ \int_{4\pi} d\hat{\Omega} \hat{\Omega} 
		Q_{ex} (\vec{r},\hat{\Omega},E,t)  
		- \int_{4\pi} d\hat{\Omega} \hat{\Omega} 
		\hat{\Omega}\bullet \nabla \psi(\vec{r},\hat{\Omega},E,t) 
		- \int_{4\pi} d\hat{\Omega} \hat{\Omega} 
		\Sigma_t \psi(\vec{r},\hat{\Omega},E,t) 
	\end{split}
\end{equation}

Now, to spare the reader from lengthy derivations, I will skip to the 
end result.

\begin{equation}
	\begin{split}
		\frac{1}{v} \frac{\partial}{\partial t} \vec{J}(\vec{r},E,t) =  
		\int_0^\infty dE' \Sigma_{s1} (\vec{r},E',t) 
		\vec{J}(\vec{r},E',t) + \vec{Q}_{ex1} - 
		\frac{1}{3} \nabla \phi (\vec{r},E,t) \\
		- \Sigma_t (\vec{r},E,t) \vec{J} (\vec{r},E,t)
	\end{split}
\end{equation}

Where, 


\begin{equation}
	\Sigma_{s1} (\vec{r},E',t) = \int_{4\pi} d\hat{\Omega} \hat{\Omega}
	\bullet \hat{\Omega}' \Sigma_s (\vec{r},E' \rightarrow E, 
	\hat{\Omega}' \bullet \hat{\Omega}) 
\end{equation}


\begin{equation}
	\vec{Q}_{ex1} =
	\int_{4\pi} d\hat{\Omega} \hat{\Omega} 
	Q_{ex} (\vec{r},\hat{\Omega},E,t)  
\end{equation}

This is done assuming a azimuthal symmetry in flux and scattering 
cross section, linearly anisotropic angular flux and linearly anisotropic 
scattering cross section, expanded using the P1 approximation. 
When external neutron sources are considered isotropic, then we 
can consider the external sources in 
the first moment equation to go to zero as well.
We also assume that our fission source and total cross 
section are isotropic.

It should be noted that the first moment equations are vector equations,
whereas the zeroth moment equations are scalar equations.

\paragraph{First Moment Equations on Continuous Energy Spectrum}

To obtain diffusion equations, we assume 
the source is isotropic so $\vec{Q}_{ex1}=0$ and that the 
variation of neutron current with time is 
small. Do note that the neutron speed v,
even for thermal neutrons is $v\approx 2200 \frac{m}{s}$ at 0.0253 eV.
Thus, it may be reasonable to assume that the variation of neutron current 
with time is small compared to the other terms. This of course assumes 
that $\frac{\partial}{\partial t} \vec{J}(\vec{r},E,t) $ is comparable 
in order of magnitude to the other terms in the equation.

We can also assume that sources within the medium are isotropic (if they 
even exist at all), so:

\begin{equation}
	Q_{ex} (\vec{r},\hat{\Omega},E,t) = 0
\end{equation}


\begin{equation}
	\frac{1}{3} (\nabla) \phi (\vec{r},E,t) =  
	\int_0^\infty dE' \Sigma_{s1} (\vec{r},E' \rightarrow E,t) 
	\vec{J}(\vec{r},E',t)  -  
	\Sigma_t (\vec{r},E,t) \vec{J} (\vec{r},E,t)
\end{equation}



\paragraph{Multigroup Diffusion Equations}

This integration over energy ranges involves the simplifying assumption 
that we can group neutrons of similar energy ranges together and not 
use a continuous energy spectrum which is more physically realistic.

\begin{equation}
	\begin{split}
		\frac{1}{v}\frac{\partial}{\partial t} 
		\phi(\vec{r},E,t) = \chi(E) 
		\int_0^\infty dE'  \nu(E') \Sigma_f (\vec{r},E') 
		\phi(\vec{r},E',t)  \\
		+ \int_0^\infty dE' \Sigma_s (\vec{r},E' \rightarrow E) 
		\phi(\vec{r},E',t) + Q_{ex} (\vec{r},E,t)  -  
		\nabla \bullet \vec{J}(\vec{r},E,t) - 
		\Sigma_t \phi(\vec{r},E,t)
	\end{split}
\end{equation}

Let us define an energy group g with an lower bound energy of 
$E_g$ and upper bound energy of $E_{g-1}$. In this definition, the 
highest energy group is group 1. To obtain the neutron 
balance over energy group g for the zeroth moment equations,

\begin{equation}
	\begin{split}
		\int_{E_{g}}^{E_{g-1}} dE
		\frac{1}{v}\frac{\partial}{\partial t} 
		\phi(\vec{r},E,t) = 
		\int_{E_{g}}^{E_{g-1}} dE
		\chi(E) 
		\int_0^\infty dE' \nu(E') \Sigma_f (\vec{r},E') 
		\phi(\vec{r},E',t) \\
		+ \int_{E_{g}}^{E_{g-1}} dE 
		\int_0^\infty dE' \Sigma_s (\vec{r},E' \rightarrow E) 
		\phi(\vec{r},E',t) + Q_{ex} (\vec{r},E,t)  -  
		\int_{E_{g}}^{E_{g-1}} dE 
		\nabla \bullet \vec{J}(\vec{r},E,t) \\ 
		- \int_{E_{g}}^{E_{g-1}} dE
		\Sigma_t \phi(\vec{r},E,t)
	\end{split}
\end{equation}

We define for each group:

\begin{equation}
	\phi_g = \int_{E_{g}}^{E_{g-1}} dE \phi(\vec{r},E,t)
\end{equation} 

\begin{equation}
	Q_{ex,g} = \int_{E_{g}}^{E_{g-1}} dE Q_{ex}(\vec{r},E,t)
\end{equation} 

\begin{equation}
	\chi_g = \int_{E_{g}}^{E_{g-1}} dE \chi(E) 
\end{equation}

Now, the flux averaged 
cross sections of type i, moment n, and group g are defined as:

\begin{equation}
	\Sigma_{i,n,g}\phi_g = \int_{E_{g}}^{E_{g-1}} dE \phi(\vec{r},E,t) 
	\Sigma_{i,n} (\vec{r},E,t)
\end{equation} 

For zeroth moment flux and cross section, I will not annotate the 
moment number. 
Also, the average number of neutrons per fission at energy E' for group 
g can be defined also:

\begin{equation}
	\nu_g \Sigma_{f,n,g}\phi_g = \int_{E_{g}}^{E_{g-1}} dE 
	\nu(E) \phi(\vec{r},E,t) 
	\Sigma_{f,n} (\vec{r},E,t)
\end{equation} 

Note that i can be any type of cross section, be it fission, or total or 
some other cross section. We also change the integral into 
a summation over G groups (where G is the number of groups) 
such that \citep{Duderstadt1976}:

\begin{equation}
	\begin{split}
		\int_{E_{g}}^{E_{g-1}} dE 
		\int_0^\infty dE' \Sigma_s (\vec{r},E' \rightarrow E) 
		\phi(\vec{r},E',t) = \\
		\sum_{g'=1}^G \int_{E_{g}}^{E_{g-1}} dE 
		\int_{E_{g'}}^{E_{g'-1}} dE' \Sigma_s (\vec{r},E' \rightarrow E) 
		\phi(\vec{r},E',t)
	\end{split}
\end{equation}

We can define the flux averaged 
group to group scattering cross section as:


\begin{equation}
	\phi_{g'} \Sigma_{s,g' \rightarrow g} = 
	\int_{E_{g}}^{E_{g-1}} dE 
	\int_{E_{g'}}^{E_{g'-1}} dE' \Sigma_s (\vec{r},E' \rightarrow E) 
	\phi(\vec{r},E',t)
\end{equation}

We also write the divergence of neutron current, which can be 
interpreted as a leakage term from the control volume, 
for energy group g to be:

\begin{equation}
	\nabla \bullet \vec{J}_g (\vec{r},t) = 
	\int_{E_{g}}^{E_{g-1}} dE \nabla \bullet \vec{J}(\vec{r},E,t)
\end{equation}

We can change the integrations into summations and so we obtain:

\begin{equation}
	\begin{split}
		\frac{1}{v}\frac{\partial}{\partial t} 
		\phi_g(\vec{r},t) = 
		\chi_g
		\sum_{g'=1}^G  \nu_{g'} \Sigma_{f,g'} (\vec{r},E') 
		\phi_{g'}(\vec{r},t) \\
		+ \sum_{g'=1}^G \phi_{g'} \Sigma_{s,g' \rightarrow g} 
		+ Q_{ex,g} (\vec{r},E,t)  -  
		\nabla \bullet \vec{J}_g (\vec{r},t) 
		- \Sigma_{t,g} \phi_g(\vec{r},t)
	\end{split}
\end{equation}

Normally, in the context of reactor physics, external sources 
are neglected, so we arrive at our multigroup version of the 
zeroth moment equations:

\begin{equation}
	\begin{split}
		\frac{1}{v}\frac{\partial}{\partial t} 
		\phi_g(\vec{r},t) = 
		\chi_g
		\sum_{g'=1}^G  \nu_{g'} \Sigma_{f,g'} (\vec{r},E') 
		\phi_{g'}(\vec{r},t) 
		+ \sum_{g'=1}^G \phi_{g'} \Sigma_{s,g' \rightarrow g} 
		-  \nabla \bullet \vec{J}_g (\vec{r},t) 
		- \Sigma_{t,g} \phi_g(\vec{r},t)
	\end{split}
\end{equation}

Now let's apply the same multigroup treatment to the 
first moment equation.
The divergence operator $\nabla \bullet$
is used on the first moment equations to convert 
it into a scalar equation. However, the implicit assumption 
in this operation is that the material is homogeneous so that 
cross sections do not vary with respect to distance position 
$\vec{r}$. This makes the diffusion equation in this form 
not as applicable with respect to variations in cross sections with 
space.

\begin{equation}
	\begin{split}
		\int_{E_{g}}^{E_{g-1}} dE
		\frac{1}{3} (\nabla \bullet \nabla) \phi (\vec{r},E,t) =  
		\int_{E_{g}}^{E_{g-1}} dE
		\int_0^\infty dE' \Sigma_{s1} (E' \rightarrow E,t) 
		\nabla \bullet \vec{J}(\vec{r},E',t)  -  \\
		\int_{E_{g}}^{E_{g-1}} dE
		\Sigma_t (E,t) \nabla \bullet \vec{J} (\vec{r},E,t)
	\end{split}
\end{equation}

It is not common to make the assumption of homogenised material 
this early in the derivation. But it makes the math easier. 
Here, rigour was not my goal, but just a 
qualitative understanding as to how assumptions can affect the final form 
of these equations.

Notice how the laplacian operator has popped up, this laplacian operator 
$(\nabla \bullet \nabla)$ appears in diffusion such as Fick's law or 
Fourier's law. Now, the integral based on neutron energy 
is one complication we need to deal with, hence, we can perform 
integration over energy groups to simplify the integral.

We can then define cross sections as weighted by the leakage term
and replace the integral for the first momment scattering 
term by a summation:

\begin{equation}
	\Sigma_{t,\nabla \bullet J, g}
	\vec{J}_g (\vec{r},t) = \int_{E_{g}}^{E_{g-1}} dE
	\Sigma_t E,t) \nabla \bullet \vec{J} (\vec{r},E,t)
\end{equation}

\begin{equation}
	\begin{split}
		\int_{E_{g}}^{E_{g-1}} dE
		\int_0^\infty dE' \Sigma_{s1} (E' \rightarrow E,t) 
		\nabla \bullet \vec{J}(\vec{r},E',t) = \\
		\sum_{g' = 1}^G 
		\int_{E_{g}}^{E_{g-1}} dE 
		\int_{E_{g'}}^{E_{g'-1}} dE' \Sigma_{s1} (E' \rightarrow E) 
		\nabla \bullet \vec{J} (\vec{r},E,t)
	\end{split}
\end{equation}

Where, 

\begin{equation}
	\nabla \bullet \vec{J}_{g'} 
	\Sigma_{s1,\nabla \bullet \vec{J},g' \rightarrow g} = 
	\int_{E_{g}}^{E_{g-1}} dE 
	\int_{E_{g'}}^{E_{g'-1}} dE' \Sigma_{s1} (E' \rightarrow E) 
	\nabla \bullet \vec{J} (\vec{r},E,t)
\end{equation}

\begin{equation}
	\begin{split}
		\frac{1}{3} (\nabla \bullet \nabla) \phi_g (\vec{r},t) =  
		\sum_{g' = 1}^G  \Sigma_{s1,\nabla \bullet \vec{J}, g' \rightarrow g} 
		\nabla \bullet \vec{J}_{g'} (\vec{r},t)
		-  
		\Sigma_{t,\nabla \bullet J, g}
		\nabla \bullet \vec{J}_g (\vec{r},t)
	\end{split}
\end{equation}

For multigroup diffusion equations specifically, we would assume that 
intergroup scattering is isotropic. This is a common assumption even 
in deriving multigroup SPN equations \citep{Fiorina2017} or even the 
energy dependent diffusion equation where we neglect anisotropic 
contribution to energy transfer \citep{Duderstadt1976}.
This means that the first moment of the scattering cross section
is zero for inscattering from other energy groups.

\begin{equation}
	\Sigma_{s1,\nabla \bullet \vec{J}, g' \rightarrow g} (g' \neq g) = 0
\end{equation}

With that, we are almost able to obtain our diffusion coefficient

\begin{equation}
	\begin{split}
		\frac{1}{3(
		\Sigma_{s1,\nabla \bullet \vec{J}, g \rightarrow g} -
		\Sigma_{t,\nabla \bullet J, g})} 
		(\nabla \bullet \nabla) \phi_g (\vec{r},t) =  
		\nabla \bullet \vec{J}_g (\vec{r},t)
	\end{split}
\end{equation}

Now, we need to make a further assumption before this equation is 
even usable. That is where flux weighted cross sections are 
approximately equal to their leakage weighted cross sections.
For this to be true we might assume that the flux experienced in a
volume element is approximately equal to the leakage of neutrons from 
that same element. 

\begin{equation}
	\phi_g (\vec{r},t) \approx \nabla \bullet \vec{J}_g (\vec{r},t)
\end{equation}

We can interpret this assumption to mean that the 
media is weakly absorbing. 
The approximation is so that 
the leakage weighted cross sections are approximately equal 
to their flux weighted counterparts.

\begin{equation}
	\Sigma_{s1,\nabla \bullet \vec{J}, g \rightarrow g} \approx 
	\Sigma_{s1, g \rightarrow g} 
\end{equation}

\begin{equation}
	\Sigma_{t,\nabla \bullet \vec{J}, g \rightarrow g} \approx 
	\Sigma_{t, g \rightarrow g} 
\end{equation}

By doing this form of hierarchical modelling, we can see that the 
diffusion approximation will not do well in strongly absorbing media 
or near discontinuities in cross sections where flux may be high but 
leakage is low. Indeed, diffusion equations do not perform well in 
these regimes \citep{Wang2018}. Nevertheless, diffusion equations 
have been used because these simplifications made the NTE solveable,
and for specific reactors and calculations, these yielded satisfactory 
results. 

The diffusion coefficient is evident in the following equation:

\begin{equation}
	\begin{split}
		- \frac{1}{3( \Sigma_{t, g} - \Sigma_{s1, g \rightarrow g} )} 
		(\nabla \bullet \nabla) \phi_g (\vec{r},t) =  
		\nabla \bullet \vec{J}_g (\vec{r},t)
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		D_g = \frac{1}{3( \Sigma_{t, g} - \Sigma_{s1, g \rightarrow g} )} 
	\end{split}
\end{equation}

We can substitute this back and use write the laplacian as 
$\nabla ^2$:

\begin{equation}
	\begin{split}
		- D_g \nabla^2 \phi_g (\vec{r},t) =  
		\nabla \bullet \vec{J}_g (\vec{r},t)
	\end{split}
\end{equation}

We then substitute it back into our zeroth moment equations to 
obtain the multigroup diffusion equations for homogenous 
multiplying media.

\begin{equation}
	\begin{split}
		\frac{1}{v}\frac{\partial}{\partial t} 
		\phi_g(\vec{r},t) = 
		\chi_g
		\sum_{g'=1}^G  \nu_{g'} \Sigma_{f,g'} 
		\phi_{g'}(\vec{r},t) \\
		+ \sum_{g'=1}^G \phi_{g'} \Sigma_{s,g' \rightarrow g} 
		+ Q_{ex,g} (\vec{r},t)  
		+ D_g \nabla^2 \phi_g (\vec{r},t) 
		- \Sigma_{t,g} \phi_g(\vec{r},t)
	\end{split}
\end{equation}

\paragraph{Point Reactor Kinetics Equations}

Of course, the multigroup diffusion equation is not quite suitable 
for solution of reactor physics in real time. For PRKE, we make 
the assumption that the neutrons have the same energy. The second 
assumption we make is that the flux solution is separable in space 
and time. This means that the flux shape will stay the same 
over time. This approach was used in textbooks \citep{Duderstadt1976}.

\begin{equation}
	\phi(\vec{r},t) = \varphi(\vec{r}) T(t)
\end{equation}

First, let us assume that there is only one energy group. 
For this, the fission spectrum $\chi = 1$. We thus arrive 
at our one group diffusion equation for homogenous multiplying media.


\begin{equation}
	\begin{split}
		\frac{1}{v}\frac{\partial}{\partial t} 
		\phi(\vec{r},t) = 
		\nu \Sigma_{f}  \phi(\vec{r},t)
		+ \Sigma_{s} \phi(\vec{r},t)
		+ Q_{ex} (\vec{r},t)  
		+ D \nabla^2 \phi (\vec{r},t) 
		- \Sigma_{t} \phi(\vec{r},t)
	\end{split}
\end{equation}

We then substitute the solution in and assume no external 
source:

\begin{equation}
	\begin{split}
		\frac{1}{v}\frac{\partial}{\partial t} 
		\varphi(\vec{r}) T(t) = 
		\nu \Sigma_{f}  \varphi(\vec{r}) T(t)
		+ \Sigma_{s} \varphi(\vec{r}) T(t)
		+ D \nabla^2 \varphi(\vec{r}) T(t)
		- \Sigma_{t} \varphi(\vec{r}) T(t)
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		\frac{1}{T(t)}\frac{\partial}{\partial t} 
		T(t) = 
		v \nu \Sigma_{f} 
		+ v \Sigma_{s} 
		+ v \frac{1}{\varphi(\vec{r})} D \nabla^2 \varphi(\vec{r}) 
		- v \Sigma_{t} 
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		\frac{1}{T(t)}\frac{\partial}{\partial t} 
		T(t) = 
		v\frac{1}{\varphi(\vec{r})}  (\nu \Sigma_{f} \varphi(\vec{r})
		+  \Sigma_{s} \varphi(\vec{r})
		+  D \nabla^2 \varphi(\vec{r}) 
		-  \Sigma_{t} \varphi(\vec{r}))
	\end{split}
\end{equation}

It is common to define a material buckling $B_m$ where:

\begin{equation}
	B_m^2 = \frac{\nu \Sigma_f - \Sigma_t + \Sigma_s}{D}
\end{equation}

So that,

\begin{equation}
	\begin{split}
		\frac{1}{T(t)}\frac{\partial}{\partial t} T(t) = 
		\frac{v}{\varphi(\vec{r})}  ( B_m^2 \varphi(\vec{r})
		+  D \nabla^2 \varphi(\vec{r}) )
	\end{split}
\end{equation}

The approach to solve for this is to equate both sides to an eigenvalue 
$-\lambda$.

\begin{equation}
	\begin{split}
		\frac{1}{T(t)}
		\frac{\partial}{\partial t} T(t) = -\lambda
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		\frac{v}{\varphi(\vec{r})}  ( B_m^2 \varphi(\vec{r})
		+  D \nabla^2 \varphi(\vec{r}) ) = -\lambda
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		( B_m^2 \varphi(\vec{r})
		+  D \nabla^2 \varphi(\vec{r}) ) 
		+ \lambda\frac{\varphi(\vec{r})}{v}  = 0
	\end{split}
\end{equation}

Here $\lambda$ is known as a time eigenvalue. There are many solutions 
to this equation, but for PRKE, we are only interested in the fundamental 
solution. This ensures that we only solve for 
one eigenvalue in the PRKE.\@ 

We define a geometric buckling $B_g$, which correlates to the 
fundamental solution as:

\begin{equation}
	B_g^2 = -\frac{1}{\varphi(\vec{r})} \nabla^2 \varphi(\vec{r}) 
\end{equation}

\begin{equation}
	\begin{split}
		B_m^2 + D B_g^2 + \frac{\lambda}{v}  = 0
	\end{split}
\end{equation}


\begin{equation}
	\begin{split}
		\lambda = -v\left(B_m^2 + D B_g^2  \right)
	\end{split}
\end{equation}

Substitution into the time varying part of the flux results in,

\begin{equation}
	\begin{split}
		\frac{1}{T(t)}
		\frac{\partial}{\partial t} T(t) = 
		v\left(B_m^2 + D B_g^2  \right)
	\end{split}
\end{equation}

We are almost at the PRKE, except that a few terms need to be 
defined as follows \citep{Duderstadt1976}:

We define an absorption cross section as:

\begin{equation}
	\Sigma_{abs} = \Sigma_t - \Sigma_s
\end{equation}

The infinite multiplication factor as:

\begin{equation}
	k_\infty  = \frac{\nu \Sigma_f}{\Sigma_{abs}}
\end{equation}

The neutron lifetime in an infinite reactor $l_\infty$ as:

\begin{equation}
	l_\infty = \frac{1}{v \Sigma_{abs}}
\end{equation}

The non leakage probability as:

\begin{equation}
	P_{NL} = \frac{1}{1+ L^2 B_g^2}
\end{equation}

Where:

\begin{equation}
	L^2 = \frac{D}{\Sigma_{abs}}
\end{equation}

The neutron lifetime in a finite reactor as:

\begin{equation}
	l = P_{NL} l_\infty = \frac{1}{v \Sigma_{abs}}
	\frac{1}{1+ L^2 B_g^2}
\end{equation}

The neutron multiplication factor as:


\begin{equation}
	\begin{split}
		k & = k_\infty P_{NL} \\ 
		& = \frac{\nu \Sigma_f}{\Sigma_{abs}}
		\frac{1}{1+ L^2 B_g^2}
	\end{split}
\end{equation}

With these we would obtain a form close to 
the point kinetics equations.

\begin{equation}
	\begin{split}
		\frac{1}{T(t)}
		\frac{\partial}{\partial t} T(t) = 
		\frac{k-1}{l}
	\end{split}
\end{equation}

If we were to multiply the spatial component (assuming that is 
solved beforehand) $\varphi(\vec{r})$, we would then obtain the 
PRKE for flux.

\begin{equation}
	\begin{split}
		\frac{\partial}{\partial t} \phi(\vec{r},t) = 
		\frac{k-1}{l}\phi(\vec{r},t)
	\end{split}
\end{equation}

We are of course, ultimately interested in reactor power. So we can 
multiply the equation by fission cross section $\Sigma_f$ and energy 
produced per fission. 

\begin{equation}
	\begin{split}
		\frac{\partial}{\partial t} P(\vec{r},t) = 
		\frac{k-1}{l}P(\vec{r},t)
	\end{split}
\end{equation}

Note here that we did not include decay heat.
The PRKE would also account for delayed neutron precursors. 
$\beta_i$ represents the delayed neutron fraction for group i 
and $C_i$ is the delayed neutron precursor concentration for 
group i with appropriate conversion factors multiplied to units 
of power. $\lambda_{decay,i}$ is the representative decay constant
for group i.

\begin{equation}
	\begin{split}
		\frac{\partial}{\partial t} P(\vec{r},t) = 
		\frac{(1-\beta)k-1}{l}P(\vec{r},t)
		+ \sum_i^n \lambda_{decay,i} C_i(\vec{r},t)
	\end{split}
\end{equation}


The decay precursors have their own balance equations where: 


\begin{equation}
	\begin{split}
		\frac{d C_i(\vec{r},t)}{d t} = \beta_i \frac{k}{l} P(\vec{r},t) - 
		\lambda_{decay,i} C_i(\vec{r},t)
	\end{split}
\end{equation}

This assumes of course that precursors do not migrate at all. In 
liquid fuelled reactors, this would be untrue.

Of course, all the delayed neutron fractions in each group must sum 
to $\beta$:

\begin{equation}
	\begin{split}
		\beta = \sum_i^n \beta_i
	\end{split}
\end{equation}

We also write the PRKE usually in terms of reactivity $\rho$ and 
mean neutron generation time $\Lambda$.

\begin{equation}
	\Lambda = \frac{l}{k}
\end{equation}

\begin{equation}
	\rho = \frac{k-1}{k}
\end{equation}

With these assumptions and definitions, we finally arrive at 
our PRKE:\@

\begin{equation}
	\begin{split}
		\frac{\partial}{\partial t} P(\vec{r},t) = 
		\frac{\rho(t) - \beta}{\Lambda}P(\vec{r},t)
		+ \sum_i^n \lambda_{decay,i} C_i(\vec{r},t)
	\end{split}
\end{equation}

\begin{equation}
	\begin{split}
		\frac{d C_i(\vec{r},t)}{d t} =  \frac{\beta_i}{\Lambda} P(r,t) - 
		\lambda_{decay,i} C_i(\vec{r},t)
	\end{split}
\end{equation}

Of course, in solving PRKE, we neglect to account for the spatial 
distribution of these precursors across the reactor. We shall just 
neglect for the time being. We would need a whole new set of assumptions 
to account for the spatial variation of these precursors. Doing a 
similar separation of variables would not really contribute to the 
discussion here. Suffice to say, however, that we would be introducing 
assumptions to simplify treatment of these precursors in a similar way 
as we derived a hierarchical surrogate model from the NTE.\@ 




Now, in the context of reactor design, we often want to have the reactor 
operate at a specified steady state. At this steady state, time varying 
component of the neutron population is zero. But do note $T(t)$ is 
non zero.

\begin{equation}
	\begin{split}
		0 = 
		\nu \Sigma_{f} 
		+ \Sigma_{s} 
		+ \frac{1}{\varphi(\vec{r})} D \nabla^2 \varphi(\vec{r}) 
		- \Sigma_{t} 
	\end{split}
\end{equation}


If we were to multiply the equation again by the flux $\phi(\vec{r},t)$,
which in this case is just $\phi(\vec{r})$ as flux is non time varying,
we get a form which 

\begin{equation}
	\begin{split}
		0 = 
		\nu \Sigma_{f} 
		+ \Sigma_{s} 
		+ \frac{1}{\varphi(\vec{r})} D \nabla^2 \varphi(\vec{r}) 
		- \Sigma_{t} 
	\end{split}
\end{equation}
