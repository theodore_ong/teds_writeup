%% step response plot
numerator = 1;
denominator = [1];
sys = tf(numerator,denominator);

% set amplitude to 2 , time delay to 3

respOpt = RespConfig('Amplitude',5, 'Delay',0.1);
[y,tOut]=step(sys,respOpt);

% convert to time data

timeData = [tOut,y,y];

% convert to frequency domain data

freqData = normalisedFFT(timeData);
peakData=linearPeakFinder(freqData);

function [freqData] = normalisedFFT(data)

% firstly, note that nufft is a non uniform FFT,
% documentation here:  https://www.mathworks.com/help/matlab/ref/double.nufft.html

% step 1: construct a proper frequency list

% first lets get the sampling frequency
n=length(data(:,1)); %no. of data points
deltaT=data(n,1)-data(1,1); % time interval from first to last data point
fs=n/deltaT; % sampling frequency

% next let's get a frequency array transposed in column form 
f=(0:n-1)*fs/n;
f=f';


% next is to prepare the data to be handled, we take out the DC component
% as well by integrating out the average
t=data(:,1);
input=data(:,2);
output=data(:,3);

DCinput=trapz(t,input)/deltaT;
DCoutput=trapz(t,output)/deltaT;

disp('average value of input is')
disp(DCinput)
disp('average value of output is')
disp(DCoutput)

% now i'll subtract the DC component out mostly
input=input-DCinput*ones(n,1);
output=output-DCoutput*ones(n,1);

% next let's get the non uniform FFT of first plot
% https://www.mathworks.com/help/matlab/ref/double.nufft.html
% to normalise the FFT, we divide by no. of data points
Y1=nufft(input,t,f)/n;
Y2=nufft(output,t,f)/n;

% now we can return the data as a frequency data array
freqData=[f,Y1,Y2];


end