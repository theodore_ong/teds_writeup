\subsection{SNF Controller Development Process}

Now that we have constructed our transfer function simulator in the 
form of the ``chem-eng-real-time-process-control-simulator'' crate written 
in Rust, we can now outline the SNF controller development process within 
the context of the Type I Digital Twin for CIET's Heater. This is to 
demonstrate how effective the Type I Digital Twin is for speeding 
up development of SNF controllers. Since we have previously mentioned 
how the speed increase was to be quantified, we now focus on the details 
of how SNF Controllers can be developed for the purposes of this work.

\subsubsection{Simulated Neutronics and Thermal Inertia Controllers}

Now, there are several approaches for SNF Controller Designs we can consider 
for this work. As mentioned in the literature review, the Desire 
Loop SNF Controller utilised PRKE based transfer functions for 
\citep{Kok1999}. These would measure the actual void fraction using 
a sensor within the DESIRE IET loop. Then the void feedback signal 
would be passed through a digital controller which would then control 
the DESIRE loop power supplies \citep{Kok1999}. The key assumption for 
SNF facilities such as the DESIRE loop is that there is thermal hydraulics 
similitude between the actual reactor and the scaled SNF facility 
\citep{Kok1999}. In this manner, we only need to add some transfer 
functions in order for the void-reactivity feedback to be accurately 
simulated \citep{Kok1999}. 

Unfortunately, for CIET, it was scaled originally to natural circulation 
flow for decay heat removal within the Pebble Bed 
Advanced High Temperature Reactor (PB-AHTR) rather than the Mark I 
PB-FHR Design \citep{DeWet2020}. However, it was then adapted for use,
with some scaling distortions, for the Mark I PB-FHR designs as it still 
matched the thermal hydraulics behaviour of the Mark I relatively closely 
\citep{Johnson2022, DeWet2020, Zweibaum2015a}. The alternative would be 
to construct another IET scaled to a more recent reactor design. However,
reactor designs change and evolve. Therefore, the use case of CIET 
may evolve over time as well. In this case, we are simulating a 
forced flow unprotected transient for an arbitrary FHR 
rather than a natural circulation flow 
for decay decay heat removal for the PB-AHTR or Mark I. We could, in 
theory, construct another IET for this purpose. 
However, constructing an IET in and of itself is 
not a small feat for an academic research laboratory. The more convenient 
option is to make do with the experimental facilities available. Therefore,
for this dissertation especially, similitude does not exist for 
thermal hydraulics since we base our reactor 
feedback on CIET. Therefore, the SNF controller would not only have to 
simulate the neutronics feedback via various means, but also the thermal 
inertia of reactor of interest. This adds an extra layer of challenge 
especially if the disparity in thermal hydraulics is large. 

One way this may be possible is that of a ``black-box'' model approach.
In this regard, we do not worry about the specifics of the thermal 
hydraulics within the reactor. Instead, we only concern ourselves to design 
a controller so that 
the scaled inlet temperature of the arbitrary FHR matches that of the 
inlet temperature of the CIET Heater, and the scaled outlet temperature 
of the arbitrary FHR matches that of the outlet temperature of CIET's 
Heater. If the arbitrary FHR has high thermal inertia, then its outlet 
temperature would change extremely slowly. If CIET's Heater has 
a low enough thermal inertia, one could design a controller such that 
CIET's Heater produces the desired outlet temperature of the arbitrary 
FHR. If the arbitrary FHR is meant to have a slow increase in 
outlet temperature, the controller can send a slowly increasing 
power signal to CIET's Heater to match this behaviour after scaling. The 
same can be said for when an outlet temperature decrease is expected. One 
could program the controller to slowly reduce the power of CIET's Heater 
such that the decrease in the outlet temperature of CIET's Heater matches 
that of the arbitrary FHR. In this case, the heater power is not scaled 
to the reactor power, and therefore its value does not hold as much 
physical meaning as before. Therefore, it is not as important to monitor 
except that it operates within safety limits and that the inlet 
temperature to outlet temperature transfer function remains the same 
as the scaled arbitrary FHR. However, there is one problem with this approach:
for some transients, the outlet temperature may be lower than the inlet 
temperature. To illustrate this, I once again present the Step Response test 
of the arbitrary FHR to a 100K increase in inlet temperature 
in Figure~\ref{fig:arb_fhr_step_response_100K}:

\begin{figure}[H]
	\begin{tikzpicture}
		\centering
		\begin{axis}[
				title = \textbf{Step Response Test 100K Inlet Temperature},
				height = 0.8\textwidth,
				width = 0.9\textwidth,
				xlabel = $Time~(s)$,
				ylabel = $Outlet~Temperature~(K)$,
				x tick label style = {
					/pgf/number format/fixed,
					/pgf/number format/fixed zerofill,
					/pgf/number format/precision=2
				},
				scaled y ticks = false, % prevents scientific notation
				%y label style={at={(-0.03,0.5)}}, % places y label at -0.02 0.5
				legend style={at={(0.02,-0.25)},anchor=south west},
			]
			%csv plot here
			%https://tex.stackexchange.com/questions/83888/how-to-plot-data-from-a-csv-file-using-tikz-and-csvsimple
			\addplot [mark = star]
			plot [error bars/.cd, x dir = both, x explicit,
			y dir = both, y explicit]
			table [x=time_seconds, 
			y=100K_step_response_kelvin, 
			col sep=comma] {./neutronics_transfer_function/stepResponse_arb_reactor_data.csv};
			\addlegendentry{GeN-Foam Data}
		\end{axis}
	\end{tikzpicture}
	\caption{Step Input of 100K applied to Inlet Temperature for 
	GeN-Foam compared to Derived Transfer Function} 
	\label{fig:arb_fhr_step_response_100K}
\end{figure}

In Figure~\ref{fig:arb_fhr_step_response_100K}, the arbitrary reactor was 
brought to a steady state where its inlet temperature was 873 K and its 
outlet temperature was around 970 K. The transient of a 100K step increase in inlet 
temperature was done at $t=0$ seconds so that the inlet temperature was 
973 K throughout. This effectively shut down the arbitrary FHR. Therefore,
for a few hours, the outlet temperature of the arbitrary FHR was lower 
than its inlet temperature likely due to negligible heat generation and 
cooler fluid still remaining within the arbitrary FHR. Therefore, the 
fluid residence time and thermal inertia contributed to the outlet coolant
temperature being significantly cooler than the inlet coolant temperature 
from $t=2000~s$ to $t=6000~s$. For a SNF Controller with thermal inertia 
simulation capabilities, this means that the outlet temperature must 
be colder than the inlet temperature for at least one hour. In this regard,
parasitic heat losses for the heater are quite necessary in order to 
reproduce this effect. Therefore, the heated section must also be able 
to cool the liquid in addition to heating the liquid up. In CIET's present 
configuration, this is not possible even with parasitic heat present when 
the heater was turned off. Hence, only a subset of transients can be 
simulated with CIET's Heater in its present state. Therefore, if SNF 
facilities were designed to simulate thermal inertia in addition to 
neutronics feedback, then we should design a heater and cooler hybrid 
to account for situations such as those in 
Figure~\ref{fig:arb_fhr_step_response_100K}. Designing a new facility 
for the purposes of SNF and simulated thermal inertia is out of scope of 
this work, but its design process would likely benefit from using 
Type I Digital Twins as well.

\subsubsection{Design Options for SNF Controllers in this Dissertation}

For now, CIET's Heater, or its digital twin, is able to simulate a 
subset of transients which require SNF capability given the right 
SNF controller. In this dissertation, I chose to use the Type I 
Digital Twin to test analogue feedforward controllers since this was 
the most conceptually straightforward. I also tested feedback PID 
controllers since the time scale for SNF due to increased inlet coolant 
temperatures was much longer than the timescale for the heater 
thermal inertia and residence time. These two design options did not 
fully reproduce the desired SNF Controller behaviour. However, designing 
a working SNF Controller is outside the scope of this dissertation. 
The main point is to show how the Type I Digital Twin of the Heater 
sped up the design iteration process. For this purpose, two design 
iterations were sufficient. Hence, I did not perform further design 
iterations for the SNF Controller. This is left for future work.

\section{Results}

After performing the two design iterations for SNF Controller using the 
Type I Digital Twin of CIET's heater, I was able to demonstrate two working 
iterations of the SNF Controller. These did not reproduce the SNF 
behaviour completely, but the PID controller was able to follow the 
reference SNF behaviour quite closely for most of the transient where 
there were no sudden changes in heater inlet temperature (BT-11). These 
two design iterations took about 21 OPC-UA Client restarts to develop in 
total over two or three working days. This equated to about 24 man hours
of work. Assuming one OPC-UA client restart equates to one successfully 
run CIET iteration experiment that took about three working days within 
one week to complete, we would require about 21 hours per person per 
successful experiment. This assumes that one working day consisted of 
seven hours of time spent in CIET and spent debugging CIET.
Since CIET required two people to operate, we can assume that about 42 
man hours of work was required per experiment. If this iterative 
process was done over 21 experiments, then we would require about 882 
man hours of work. Based on this estimate, designing the SNF Controller 
using the Type I Digital Twin of CIET's Heater was roughly 21 times more 
efficient in terms of working hours. Also, roughly a total of 861 man hours 
were saved when performing the first two design iterations on the 
Type I Digital Twin. Of course, designing a SNF Controller would require 
many more iterations than these two design iterations, but this would 
only further prove that the Type I Digital Twin is an essential part 
of the design process if one wanted to be efficient with time.

Of course, to write code for this Type I Digital Twin from the ground up, 
it took roughly two years of trial and error. Nevertheless, I conjecture 
that the time spent in these two years would not only help to save my own 
time should I want to continue writing SNF controllers, but would also 
help others if they want to do something similar. Thus, this was time 
well spent. Additionally, several lessons were learnt during the first 
two design iterations. These lessons, along with the design iteration 
results are presented in the following subsections.

\subsection{SNF Development Initial Results}

Let us now discuss some of the initial results for how the SNF controller 
performed and some of the important learning points.
Using the aforementioned methodology, I started developing my SNF 
controller using the Type I Digital Twin Rust Server and the 
Rust OPC-UA client. I took approximately 13 iterations to get my initial 
results as I had to debug various issues and configure the workings 
of the transfer function library and csv exports correctly. This was 
meant to mimic the development process I would have taken if I used 
ARCO-CIET to program my SNF controller. 

Unsurprisingly, my first results for the SNF Controller were not 
particularly successful. When I decreased
the inlet temperature of the heater (BT-11), the outlet temperature 
(BT-12) ought to have increased after sometime due to higher reactor 
power output. Nevertheless, erroneous behaviour and various bugs caused
the SNF controller to switch off the heater after some time despite the 
fact that the heater was meant to increase power output. This behaviour 
is shown in Figure~\ref{fig:reactor_feedback_initial_results_13_iterations}:

\begin{figure}[H]
	\centering
	\begin{tikzpicture}
		\pgfplotsset{
			%scale only axis,
			%xmin=-0.01, xmax=0.20,
			%x tick label style = {
			%	/pgf/number format/fixed,
			%	/pgf/number format/fixed zerofill,
			%	/pgf/number format/precision=2
			%},
			scaled y ticks = false, % prevents scientific notation
			width = 0.8\textwidth,
			legend style={at={(0.02,-0.35)},anchor=south west},
		}
		\begin{axis}[
				title = Initial Results of SNF Controller Development,
				xlabel = Time (s),
				ylabel = Temperature $(^\circ C)$,
			]
			%csv plot here
			%https://tex.stackexchange.com/questions/83888/how-to-plot-data-from-a-csv-file-using-tikz-and-csvsimple
			\addplot [only marks, mark = asterisk]
			plot [error bars/.cd, 
			y dir = both, y explicit]
			table [x=time_seconds, 
				y=outlet_temp_desired_transfer_fn, 
				col sep=comma,
			] {./snf_controller_dev/snf_dev_iterations/reactor_feedback_initial_results_13_iterations.csv};
			\addlegendentry{Desired Output Temperature by SNF Transfer 
			Function}
			\addplot [no marks]
			plot [error bars/.cd, 
			y dir = both, y explicit]
			table [x=time_seconds, 
				y=outlet_temp, 
				col sep=comma,
			] {./snf_controller_dev/snf_dev_iterations/reactor_feedback_initial_results_13_iterations.csv};
			\addlegendentry{BT-12 Outlet Temperature}

			\addplot [no marks, dashed]
			plot [error bars/.cd, 
			y dir = both, y explicit]
			table [x=time_seconds, 
				y=inlet_temp, 
				col sep=comma,
			] {./snf_controller_dev/snf_dev_iterations/reactor_feedback_initial_results_13_iterations.csv};
			\addlegendentry{BT-11 Inlet Temperature}
		\end{axis}
	\end{tikzpicture}
	\caption{Initial Postprocessing Results after 13 Iterations} 
	\label{fig:reactor_feedback_initial_results_13_iterations}
\end{figure}

In Figure~\ref{fig:reactor_feedback_initial_results_13_iterations},
the heater outlet temperature was initially set to around 80 $^\circ C$.
However, it took about 50 seconds for the heater to reach a steady state 
coolant outlet temperature of 102.24$ ^\circ C$. At roughly 100 seconds, 
I reduced the inlet temperature of the coolant. This should have gotten 
the outlet coolant temperature to increase due to increased reactor power 
output. However, the SNF controller was not able to compensate for the 
decrease in inlet temperature. This is because the SNF Controller 
transfer function did not account for the decrease in inlet coolant 
temperature. This was an oversight on my part, but it showed the need 
for iterative design processes. Additionally, after the initial response,
the SNF controller essentially lowered the heater power 
until it was 0 $kW$. This was puzzling, but at least the basic data logging 
functions and other basic functions were working correctly. I did not 
manage to debug the root cause of this issue until the second design 
iteration was complete.

These first 13 iterations took approximately one day. If this iteration 
process was done in ARCO-CIET, the approximate time, assuming one could 
find the right schedules and people for the experiment, would have been 
roughly 13 weeks or almost one semester. While the SNF controller was not 
successful in that it did not produce the correct SNF behaviour, it showed 
that the timescales for the SNF transients due to changes in inlet 
coolant temperature were much longer than the timescales for transient 
conjugate heat transfer (CHT) within the heater. This is because the 
thermal inertia of the heater is very low in comparison to the thermal 
inertia of the fluid, at least compared to typical ratios found within 
FHRs and the arbitrary FHR. Given that this is the case, I opted to use 
a simpler PID controller in order to produce the desired SNF behaviour 
rather than debug the root cause of this error.

The key learning point for this design iteration was that I needed to 
consider a state space representation for the SNF Controller transfer 
function or state space model. This would have allowed for a control 
scheme with multiple inputs, and allowed me one more transfer function 
to try and reject the decrease in outlet temperature brought about 
by decrease in inlet temperature.

\subsection{PID Type SNF Controller}

Given this state of affairs, I decided to try the PID type SNF controller.
In this setup, the SNF transfer function would determine the set point 
for the heater outlet temperature BT-12. Given such a set point, the 
controller would control the heater power so that the error between 
the Digital Twin's BT-12 temperature and the set point was minimised.
In this regard, the SNF behaviour would have been adequately replicated 
because the timescales were so long. To attain the results for the second 
design using PID, approximately eight attempts were needed. This took around 
one to two working days because of the several hour long times for the transients.

When I iteratively designed the PID controller, I found 
severe limitations when using PID controllers acting 
on the error. As expected, the PID controller produced severe oscillations 
seen during the verification tests because of the time discretisation issue.
In ARCO-CIET and the Type I Digital Twin, temperature samples were 
taken every 0.1 seconds when using digital control. Therefore,
it behaved more like a discretised (digital) time system simulation 
rather than a continuous time system simulation. This caused 
oscillatory behaviour within the Type I Digital Twin of the heater 
during early iterations of the PID controller shown in 
Figure~\ref{fig:pid_controller_debugging_with_gui}:

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{
	./snf_controller_dev/snf_dev_iterations/pid_controller_debugging_with_gui.png}
	\caption{Oscillatory Behaviour Observed during Early Iterations of 
	PID Control based SNF}
	\label{fig:pid_controller_debugging_with_gui}
\end{figure}

Moreover, Figure~\ref{fig:pid_controller_debugging_with_gui} showed 
inherent limitations in having the heater power at a maximum of 10 kW. 
This is because transients were performed on the heater when it was 
operating at a steady state of 8 kW. At 8 kW, the heater's behaviour 
was validated with experimental data. Therefore, I could perform 
transients from this steady state knowing that the heater behaved 
reasonably according to experimental data. Ideally, I would have operated 
the heater at a lower power and flowrate, around 4 or 5 kW, and 
performed transient studies from that state. However, this would require 
a different set of experimental validation studies for the Type I Digital 
Twin. I left this to future work as this did not directly contribute to 
the purpose of this dissertation.

Now, to remove this oscillatory behaviour, I decided to follow Seborg's 
suggestion and place the derivative element within the feedback portion 
of the control loop in a PI - PD version of the PID controller 
\citep{Seborg2016}. The block diagram of which is shown in 
Figure~\ref{fig:pi-pd-block-diagram}:

%%%%% for drawing controllers

\tikzstyle{block} = [draw, fill=blue!20, rectangle, 
    minimum height=3em, minimum width=6em]
\tikzstyle{sum} = [draw, fill=blue!20, circle, node distance=1cm]
\tikzstyle{input} = [coordinate]
\tikzstyle{output} = [coordinate]
\tikzstyle{pinstyle} = [pin edge={to-,thin,black}]

%%%% see website: https://texample.net/tikz/examples/control-system-principles/
%%%%%%%

% The block diagram code is probably more verbose than necessary
\begin{figure}[H]
	\centering
	\begin{tikzpicture}[auto, node distance=2cm]
	% We start by placing the blocks
		\node [input, name=input] {};
		\node [sum, right of=input, node distance = 3cm] (sum) {};
		\node [block, right of=sum, node distance = 2.25 cm] (controller) {$G_{PI}(s)$};
		\node [block, right of=controller, 
		node distance=5cm] (heater) {CIET Heater};
	% We draw an edge between the controller and heater block to 
	% calculate the coordinate u. We need it to place the measurement block. 
		\draw [->] (controller) -- node[name=u] {$u(t)$} (heater);
		\draw [->] (sum) -- node[name=e] {$e$} (controller);
		\node [output, right of=heater, node distance = 4cm] (output) {};
		\node [block, below of=heater, node distance = 5cm] (measurements) {$G_m (s)$};
		\node [block, left of=measurements, node distance = 5 cm] (pd_controller) {$G_{PD} (s)$};

	% Once the nodes are placed, connecting them is easy. 
		\draw [draw,->] (input) -- node {$T_{outlet~sp}$} (sum);
		\draw [->] (heater) -- node [name=y] {$T_{outlet}$}(output);
		\draw [->] (y) |- (measurements);
		\draw [->] (measurements) -- (pd_controller);
		\draw [->] (pd_controller) -| node[pos=0.99] {$-$} 
		node [near end] {$y_m$} (sum);
	\end{tikzpicture}
	\caption{PI-PD Feedback Controller Diagram}
	\label{fig:pi-pd-block-diagram}
\end{figure}

In Figure~\ref{fig:pi-pd-block-diagram}, $T_{outlet~sp}$ is the outlet 
temperature set point determined by the SNF transfer function. $T_{outlet}$
is the Heater Outlet Temperature BT-12. $G_m(s)$ is the measurement 
transfer function which should be 1 if we were to ignore the lags and 
delays between OPC-UA server and client.
Based on Figure~\ref{fig:pi-pd-block-diagram}, I define a transfer 
function for the PI and PD controller:

\begin{equation}\label{eqn:pd-controller}
	G_{PD} = \frac{3.339 s}{0.1 \times 3.339 s +1} + 1
\end{equation}

\begin{equation} \label{eqn:pi-controller}
	G_{PI} = 80.0 \frac{watts}{K}
	\left( \frac{1}{7s} + 1 \right)
\end{equation}

In both equations \ref{eqn:pd-controller} and \ref{eqn:pi-controller}, the 
proportional and integral time were in units of seconds. Both equations 
\ref{eqn:pd-controller} and \ref{eqn:pi-controller} were initially estimated 
using Chien and Fruehauf's internal model control (IMC) tuning 
correlations \citep{Fruehauf1994} for PID controller tunings found 
in textbooks \citep{Seborg2016}. While Chien and Fruehauf's correlations 
were meant for PID controllers in parallel forms, they served as decent 
initial estimates for the PI-PD controller that I developed.
This PI-PD controller
approach worked well in removing oscillations within the outlet 
temperature. Early versions of the PI-PD version of the PID controller
showed that the controller was able to get the outlet temperature of BT-12 
to follow the desired output given by the transfer function at least for 
the longer term. However, the PI-PD controller was not able to reject 
the initial disturbances in the simulated BT-12 temperatures due to 
increased heater inlet temperatures (BT-11). This is shown in 
Figure~\ref{fig:sim_reactivity_feedback_decent_test_1_final_seven_mistake}:

\begin{figure}[H]
	\centering
	\begin{tikzpicture}
		\pgfplotsset{
			%scale only axis,
			%xmin=-0.01, xmax=0.20,
			%x tick label style = {
			%	/pgf/number format/fixed,
			%	/pgf/number format/fixed zerofill,
			%	/pgf/number format/precision=2
			%},
			scaled y ticks = false, % prevents scientific notation
			width = 0.8\textwidth,
			legend style={at={(0.02,-0.35)},anchor=south west},
		}
		\begin{axis}[
				title = Initial Results of PID SNF Controller Development,
				xlabel = Time (s),
				ylabel = Temperature $(^\circ C)$,
			]
			%csv plot here
			%https://tex.stackexchange.com/questions/83888/how-to-plot-data-from-a-csv-file-using-tikz-and-csvsimple
			\addplot [only marks, mark = asterisk]
			plot [error bars/.cd, 
			y dir = both, y explicit]
			table [x=time_seconds, 
				y=simulated_reactivity_feedback_set_point, 
				col sep=comma,
			] {./snf_controller_dev/snf_dev_iterations/sim_reactivity_feedback_decent_test_1_final_7seven_iterations_mistake_with_controller.csv};
			\addlegendentry{Desired Output Temperature set by SNF Transfer 
			Function}
			\addplot [no marks]
			plot [error bars/.cd, 
			y dir = both, y explicit]
			table [x=time_seconds, 
				y=bt12_simulated_outlet_temp, 
				col sep=comma,
			] {./snf_controller_dev/snf_dev_iterations/sim_reactivity_feedback_decent_test_1_final_7seven_iterations_mistake_with_controller.csv};
			\addlegendentry{BT-12 Outlet Temperature}

			\addplot [no marks, dashed]
			plot [error bars/.cd, 
			y dir = both, y explicit]
			table [x=time_seconds, 
				y=inlet_temp, 
				col sep=comma,
			] {./snf_controller_dev/snf_dev_iterations/sim_reactivity_feedback_decent_test_1_final_7seven_iterations_mistake_with_controller.csv};
			\addlegendentry{BT-11 Inlet Temperature}
		\end{axis}
	\end{tikzpicture}
	\caption{Initial Postprocessing Results for PI-PD Controller} 
	\label{fig:sim_reactivity_feedback_decent_test_1_final_seven_mistake}
\end{figure}

The initial overshoot was due to the heater requiring some settling time as 
it reached an initial steady state. The latter overshoot was due to an increase in 
heater inlet temperature where the feedback controller was unable to reject 
the disturbance. Moreover, the transient timescale in 
Figure~\ref{fig:sim_reactivity_feedback_decent_test_1_final_seven_mistake}
was wrong because of a bug. The simulated reactivity feedback transfer function 
was mistakenly using the difference between the heater outlet temperature 
and steady state inlet temperature reading $T_{outlet}-T_{inlet~steady~state}$
to calculate the desired heater outlet temperature. This means that 
the transient shown was a result of mistakenly overpredicting simulated 
reactivity feedback. Thus, the transient time scale in 
Figure~\ref{fig:sim_reactivity_feedback_decent_test_1_final_seven_mistake}
was significantly shorter than the timescale of the correct SNF 
behaviour. For correct SNF behaviour, we should expect the time 
scale to dip after several hours rather than within one hour. 

Despite this, 
Figure~\ref{fig:sim_reactivity_feedback_decent_test_1_final_seven_mistake}
showed that the PI-PD SNF controller was able to keep pace with the 
calculated set point from the SNF transfer function scaled down from 
GeN-Foam for the most part of the transient. Therefore, aside from 
the $T_{outlet}-T_{inlet~steady~state}$ issue, I was largely satisfied 
with the design. I then took some action to correct 
the $T_{outlet}-T_{inlet~steady~state}$ issue. This was largely 
successful, and a second transient was plotted in 
Figure~\ref{fig:sim_reactivity_feedback_decent_test_2_step_change_1_iteration}:


\begin{figure}[H]
	\centering
	\begin{tikzpicture}
		\pgfplotsset{
			%scale only axis,
			%xmin=-0.01, xmax=0.20,
			%x tick label style = {
			%	/pgf/number format/fixed,
			%	/pgf/number format/fixed zerofill,
			%	/pgf/number format/precision=2
			%},
			scaled y ticks = false, % prevents scientific notation
			width = 0.8\textwidth,
			legend style={at={(0.02,-0.35)},anchor=south west},
		}
		\begin{axis}[
				title = Results of PI-PD Style PID SNF Controller Development,
				xlabel = Time (s),
				ylabel = Temperature $(^\circ C)$,
			]
			%csv plot here
			%https://tex.stackexchange.com/questions/83888/how-to-plot-data-from-a-csv-file-using-tikz-and-csvsimple
			\addplot [only marks, mark = asterisk]
			plot [error bars/.cd, 
			y dir = both, y explicit]
			table [x=time_seconds, 
				y=simulated_reactivity_feedback_set_point, 
				col sep=comma,
			] {./snf_controller_dev/snf_dev_iterations/sim_reactivity_feedback_decent_test_2_step_change_1_iteration.csv};
			\addlegendentry{Desired Output Temperature set by SNF Transfer 
			Function}
			\addplot [no marks]
			plot [error bars/.cd, 
			y dir = both, y explicit]
			table [x=time_seconds, 
				y=bt12_simulated_outlet_temp, 
				col sep=comma,
			] {./snf_controller_dev/snf_dev_iterations/sim_reactivity_feedback_decent_test_2_step_change_1_iteration.csv};
			\addlegendentry{BT-12 Outlet Temperature}

			\addplot [no marks, dashed]
			plot [error bars/.cd, 
			y dir = both, y explicit]
			table [x=time_seconds, 
				y=inlet_temp, 
				col sep=comma,
			] {./snf_controller_dev/snf_dev_iterations/sim_reactivity_feedback_decent_test_2_step_change_1_iteration.csv};
			\addlegendentry{BT-11 Inlet Temperature}
		\end{axis}
	\end{tikzpicture}
	\caption{Postprocessing Results for PI-PD Controller} 
	\label{fig:sim_reactivity_feedback_decent_test_2_step_change_1_iteration}
\end{figure}

Figure~\ref{fig:sim_reactivity_feedback_decent_test_2_step_change_1_iteration} 
shows the correct SNF behaviour over the correct timescale. Other than 
the inability of the controller to reject the initial increase in outlet 
temperature, the SNF controller has generally performed well. While, 
I could make a third design iteration to eliminate the initial 
increase in outlet temperature, designing a SNF controller that perfectly 
mimics the SNF behaviour is not necessary for this work. Instead, I was 
only aiming to show that this iterative design process for the SNF controller 
was expedited by the Type I Digital Twin. This goal has been achieved, and 
therefore, I decided to leave the actual design of the 
SNF controller to future work.

\section{Future Work}

Based on the experiences of the two previous design iterations of 
the SNF controller, we can clearly see that the Type I Digital Twin 
of CIET's heater performs a vital role in expediting the process.
Given that we now have the tools with which to iteratively design the SNF 
controller, we could perform more design iterations so that the 
SNF controller is able to reject the initial increase in outlet 
temperature due to increase in heater inlet temperature. 
Additionally, we might explore other designs which incorporate control 
rod movement and other feedback mechanisms into the SNF controller. 

Moreover, based on the time scale of the SNF transient, we 
may even upgrade the arbitrary FHR simulation to include decay heat and 
Xenon-135 transients, or even base these simulations on the gFHR 
\citep{Kile2022}. Additionally, the surrogate models we use for modelling 
SNF behaviour could also be improved beyond transfer functions and state 
space models. We could use something more sophisticated such as neural 
networks or even a low fidelity deterministic lattice code. 

% checked 17 jan 2024
