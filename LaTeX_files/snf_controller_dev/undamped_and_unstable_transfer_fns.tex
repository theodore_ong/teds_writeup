Now, in my transfer function control module, the undamped and unstable 
cases are out of scope for the main section of the dissertation. However,
a transfer function control module isn't quite complete if these 
cases are not simulated.

\subsection{Undamped Cases}

For the undamped cases, the transfer function would usually come 
in the form of sines and cosines.

\begin{equation}
	G(s) = \frac{K_{p1} \omega}{s^2 + \omega^2} + 
	\frac{K_{p2} s}{\omega^2 + s^2}
\end{equation}

Now, if we apply a step function $1/s$:

\begin{align*}
	Y(s) &= \frac{1}{s} \frac{K_{p1} \omega}{s^2 + \omega^2} + 
	\frac{1}{s}\frac{K_{p2} s}{\omega^2 + s^2} \\ 
	&= \frac{1}{s} \frac{K_{p1} \omega}{s^2 + \omega^2} + 
	\frac{K_{p2} }{\omega^2 + s^2} \\
	&= \frac{A}{s} + \frac{Bs + C}{s^2 + \omega^2} + 
	\frac{K_{p2} }{\omega^2 + s^2} \\
\end{align*}

Now, to find, $A$, $B$ and $C$, we use partial fractions

\begin{align*}
	& \frac{A}{s} + \frac{Bs + C}{s^2 + \omega^2}	 
	=\frac{1}{s} \frac{K_{p1} \omega}{s^2 + \omega^2} \\
	& \frac{As^2 + A\omega^2}{s(s^2+ \omega^2)} + \frac{Bs^2 + Cs}{s(s^2 + \omega^2)}	 
	=\frac{1}{s} \frac{K_{p1} \omega}{s^2 + \omega^2} \\
\end{align*}

Comparing coefficients, we have:

\begin{equation*}
	A \omega^2 = K_{p1}\omega
\end{equation*}

\begin{equation*}
	Cs = 0
\end{equation*}

\begin{equation*}
	As^2 + Bs^2 = 0
\end{equation*}

Now, $C = 0$ and $A = \frac{K_{p1}}{\omega}$, hence:

\begin{equation*}
	\frac{K_{p1}}{\omega}s^2 + Bs^2 = 0
\end{equation*}

Therefore, $B = - \frac{K_{p1}}{\omega}$. And so:

\begin{align*}
	Y(s) &= \frac{1}{s} \frac{K_{p1} \omega}{s^2 + \omega^2} + 
	\frac{1}{s}\frac{K_{p2} s}{\omega^2 + s^2} \\ 
	&= \frac{1}{s} \frac{K_{p1} \omega}{s^2 + \omega^2} + 
	\frac{K_{p2} }{\omega^2 + s^2} \\
	&= \frac{A}{s} + \frac{Bs + C}{s^2 + \omega^2} + 
	\frac{K_{p2} }{\omega^2 + s^2} \\
	&= \frac{A}{s} + \frac{Bs + C}{s^2 + \omega^2} + 
	\frac{K_{p2} }{\omega^2 + s^2} \\
	&= \frac{K_{p1}}{\omega s} - \frac{1}{\omega}\frac{K_{p1} s }{s^2 + \omega^2} + 
	\frac{K_{p2} }{\omega^2 + s^2} \\
	&= \frac{1}{\omega}
	\left[ \frac{K_{p1}}{ s} - \frac{K_{p1} s }{s^2 + \omega^2} + 
	\frac{K_{p2} \omega }{\omega^2 + s^2}
	\right]  \\
\end{align*}

If the step happens to have magnitude $a_0$, and starting at time $t = c$:
\begin{align*}
	Y(s) &= \frac{a_0}{s} \exp(-c s) \frac{K_{p1} \omega}{s^2 + \omega^2} + 
	\frac{1}{s}\frac{K_{p2} s}{\omega^2 + s^2} \\ 
	&= \frac{a_0}{\omega}\exp(-c s) 
	\left[ \frac{K_{p1}}{ s} - \frac{K_{p1} s }{s^2 + \omega^2} + 
	\frac{K_{p2} \omega }{\omega^2 + s^2}
	\right]  \\
\end{align*}

We can see that the step function would produce a proportional step 
change of magnitude, as well as changes to the sine and cosine amplitudes.
Programmatically, the sinusoids never decay. However, the magnitudes 
of the step changes, as well as the sines and cosines would change 
accordingly. There will be no vector storing changes. Rather, changes 
of the offset, as well as the sine and cosine amplitudes will be changed 
accordingly depending on the step changes. The algorithm will be quite 
similar to the integral controller.

\subsection{Unstable First Order Transfer Function}

For a first order unstable transfer function, the frequency domain 
representation is:

\begin{equation}
	G(s) = \frac{K_p}{\tau_p s - 1}
\end{equation}

Given a Heaviside function with amplitude $a_0$ and at time $t=c$:

\begin{align*}
	Y(s) &= \exp(-cs) \frac{a_0}{s} \frac{K_p}{\tau_p s - 1}\\
	&= \exp(-cs)a_0 \left[ \frac{1}{s} \frac{K_p}{\tau_p s - 1} \right]\\
	&= \exp(-cs)a_0 \left[ \frac{A}{s} + \frac{B}{\tau_p s - 1} \right]\\
\end{align*}

Comparing coefficients:

\begin{align*}
	& A(\tau_p s - 1) + B s = K_p \\
	& A\tau_p s - A  + B s = K_p \\
\end{align*}

From this, $A = -K_p$ and $B = K_p \tau_p$

\begin{align*}
	Y(s) &= \exp(-cs) \frac{a_0}{s} \frac{K_p}{\tau_p s - 1}\\
	&= \exp(-cs)a_0 \left[ -\frac{K_p}{s} + \frac{K_p \tau_p}{\tau_p s - 1} \right]\\
\end{align*}

In the time domain:

\begin{align*}
	& Y(s) = \exp(-cs)a_0 \left[ -\frac{K_p}{s} + \frac{K_p \tau_p}{\tau_p s - 1} \right]\\
	& Y(t) = a_0  u(t-c) \left[ - K_p +  K_p \exp(\tau_p t) \right] \\
\end{align*}

%\todo{probably check}

For a series of step inputs:

\begin{align*}
	& Y(t) = a_0  u(t-c) \left[ - K_p +  K_p \exp(\tau_p t) \right] \\
	& Y(t) = \sum_{i=1}^N a_i  u(t-c_i) \left[ - K_p +  K_p \exp(\tau_p t) \right] \\
	& Y(t) = - \sum_{i=1}^N a_i K_p u(t-c_i) +
	\sum_{i=1}^N a_i  u(t-c_i)  K_p \exp(\tau_p t)  \\
	& Y(t) = -K_p \sum_{i=1}^N a_i  u(t-c_i) + K_p\exp(\tau_p t)
	\sum_{i=1}^N a_i  u(t-c_i)     \\
\end{align*}


Again, the exponents never decay away, but much like the integral gain,
one does not need to store the responses in vectors. One only needs to change 
the magnitude of the exponential terms plus whatever offset there is.

I probably need to recheck my math though\ldots

\subsection{Unstable Second Order Transfer Function}


