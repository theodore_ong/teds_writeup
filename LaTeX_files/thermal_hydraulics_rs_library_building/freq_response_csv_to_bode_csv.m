
%% this file contains a function that takes in the name of the csv file 
% where the first column is time, the second column is input, and the 
% third column is output, and converts it to a csv plot ready for bode 
% plots

function freq_response_csv_to_bode_csv(name_of_frequ_response_csv_file, ...
    name_of_bode_plot_csv_file)

freqData=myFFT(name_of_frequ_response_csv_file);
peakData=linearPeakFinder(freqData);
freq_resp_data_from_peak = peakData2idfrd(peakData);
[mag,phase,omega_out] = bode(freq_resp_data_from_peak);


mag = squeeze(mag);
mag = 20*log10(mag);
phase = squeeze(phase);

frequency_out = 0.5/pi *omega_out;
gainAndPhase_data = [frequency_out,mag,phase];

% need to trim data down

plot_table_simulated = array2table(gainAndPhase_data);
plot_table_simulated.Properties.VariableNames = {'frequency_Hz' , 'mag_dB', 'phase'};

% name csv file properly 
name_of_bode_plot_csv_file = strcat(name_of_bode_plot_csv_file,'.csv');
writetable(plot_table_simulated, name_of_bode_plot_csv_file);
end

%% List of Functions

function [fr_data]=peakData2idfrd(peakData)
%% this function takes multiple bode plots and creates a idfrd object which can be used to estimate
% a transfer function

%% one would obtain several plots of data first

frequencySize = size(peakData.peakFrequenciesCSV);
frequencySize = frequencySize(1)/2;

freq=peakData.peakFrequenciesCSV(1:frequencySize);
response=peakData.complexGainCSV(1:frequencySize);




%% now i load in my sampling interval (estimated)

Ts = peakData.samplingInterval_s;


% note that sampling time here is just to help eliminate any data point
% with frequency above nyquist frequency


% lastly create the idfrd object
% https://www.mathworks.com/help/ident/ref/idfrd.html#f4-1777348

fr_data=idfrd(response,freq,Ts,'FrequencyUnit','Hz');
%fr_data=idfrd(response,freq,Ts);

end

function [y,t] = test_transfer_fn(transfer_function_sys,amplitude)
% first some config for the transfer function
Config = RespConfig('InputOffset',0,'Amplitude',amplitude,'Delay',0);
[y,t] = step(transfer_function_sys,Config);

end

function [peakData] = linearPeakFinderPlotOverlay(freqData)
% basically i'm finding the peaks with their associated frequencies in this
% function, i output an object with both input and output peaks

%% first
% let's import the data from freqData to make it more readable
f=freqData(:,1);
input=freqData(:,2);
output=freqData(:,3);

%% second find the magnitude of the peaks

inputMag=abs(input);
outputMag=abs(output);

% decibels here will be used as the threshold for the peak prominence

thresholdInput=max(abs(inputMag))/20;
thresholdOutput=max(abs(outputMag))/20; % only input peaks are used 



%% third let's calculate our input and output peaks and frequencies
% https://www.mathworks.com/help/signal/ref/findpeaks.html

% load them straightaway into the data structure

[peakData.inputPeaksMag,peakData.inputPeakFreqs]=findpeaks(inputMag,f,'MinPeakHeight',thresholdInput);

[peakData.outputPeaksMag,peakData.outputPeakFreqs]=findpeaks(outputMag,f,'MinPeakHeight',thresholdOutput);

% now i also want to load frequency and complex gain into the peak finder

[pks,inputPeakIndex]=findpeaks(inputMag,'MinPeakHeight',thresholdInput);
clear pks

% this next part extracts the peaks at the various frequencies according to ONLY the input peaks
inputPeaks=elementSearch(input,inputPeakIndex);
outputPeaks=elementSearch(output,inputPeakIndex);
peakFrequencies=elementSearch(f,inputPeakIndex);

complexGain=outputPeaks./inputPeaks;

peakData.peakFrequenciesCSV=peakFrequencies;
peakData.complexGainCSV=complexGain;


%% fourth, print the results
disp('use clf to clear the figures')
findpeaks(inputMag,f,'MinPeakHeight',thresholdInput);
hold on
findpeaks(outputMag,f,'MinPeakHeight',thresholdOutput);
disp('remember to also set hold off')
hold off
end
%% the element search function used to isolate peaks based on location is here
% it takes in two vectors, 

% first vector is a vector of FFT data against some index
% second vector is a vector of indices
% the output vector just gives a vector of indices at that location

function [filteredFFTData]=elementSearch(FFTData,inputPeakIndices)

n=length(inputPeakIndices);
filteredFFTData=zeros(n,1);


for i=1:n
    peakIndex=inputPeakIndices(i,1);
    filteredFFTData(i,1)=FFTData(peakIndex,1);
end

end

%% function for exporting bode plots as png

% this will take a filename as string, current figure handle (use gcf to
% get current figure)
% and plot title as string

function export_bode_plot_as_png(bodeplot_handle,file_name,plot_title)

file_type = 'png';

% now resize the plots
bode_options = getoptions(bodeplot_handle);
bode_options.grid = 'on';
bode_options.Title.String = plot_title;
setoptions(bodeplot_handle,bode_options);


current_figure = gcf;

% resize the plot

x0=700;
y0=280;
width=800;
height=600;
set(current_figure,'position',[x0,y0,width,height]);


% export plot

saveas(current_figure,file_name,file_type);
end
function [freqData] = myFFT(csvFile)
% this is a custom FFT Script that takes time domain data 
% one input and one output, and churns out gain and phase data


%   Detailed explanation goes here


% first we import the csv file, of course greet the user first too...

data=csv2array(csvFile);
printImportcsv();

% now we have csv data, we'll need to do a nufft on it to churn out
% normalised FFT of our data

freqData=normalisedFFT(data);

end



function [] = printImportcsv()

disp('imported csv file to array...')

end

function [array] = csv2array(csv)

% first let's import the table, using readTable, 
% https://www.mathworks.com/matlabcentral/answers/72545-how-to-import-csv-file-in-matlab
tableData = readtable(csv);

% then i'll convert the table data into a double array
% https://www.mathworks.com/matlabcentral/answers/370544-how-do-i-convert-table-data-to-double-to-manipulate-them
array = table2array(tableData);


end

%% here are functions actually doing FFT

function [freqData] = normalisedFFT(data)

% firstly, note that nufft is a non uniform FFT,
% documentation here:  https://www.mathworks.com/help/matlab/ref/double.nufft.html

% step 1: construct a proper frequency list

% first lets get the sampling frequency
n=length(data(:,1)); %no. of data points
deltaT=data(n,1)-data(1,1); % time interval from first to last data point
fs=n/deltaT; % sampling frequency

% next let's get a frequency array transposed in column form 
f=(0:n-1)*fs/n;
f=f';


% next is to prepare the data to be handled, we take out the DC component
% as well by integrating out the average
t=data(:,1);
input=data(:,2);
output=data(:,3);

DCinput=trapz(t,input)/deltaT;
DCoutput=trapz(t,output)/deltaT;

disp('average value of input is')
disp(DCinput)
disp('average value of output is')
disp(DCoutput)

% now i'll subtract the DC component out mostly
input=input-DCinput*ones(n,1);
output=output-DCoutput*ones(n,1);

% next let's get the non uniform FFT of first plot
% https://www.mathworks.com/help/matlab/ref/double.nufft.html
% to normalise the FFT, we divide by no. of data points
Y1=nufft(input,t,f)/n;
Y2=nufft(output,t,f)/n;

% now we can return the data as a frequency data array
freqData=[f,Y1,Y2];


end

function [peakData] = linearPeakFinder(freqData)
% basically i'm finding the peaks with their associated frequencies in this
% function, i output an object with both input and output peaks

%% first
% let's import the data from freqData to make it more readable
f=freqData(:,1);
input=freqData(:,2);
output=freqData(:,3);

%% second find the magnitude of the peaks

inputMag=abs(input);
outputMag=abs(output);

% decibels here will be used as the threshold for the peak prominence

thresholdInput=max(abs(inputMag))/4;
thresholdOutput=max(abs(outputMag))/10; % only input peaks are used 



%% third let's calculate our input and output peaks and frequencies
% https://www.mathworks.com/help/signal/ref/findpeaks.html

% load them straightaway into the data structure

[peakData.inputPeaksMag,peakData.inputPeakFreqs]=findpeaks(inputMag,f,'MinPeakHeight',thresholdInput);

[peakData.outputPeaksMag,peakData.outputPeakFreqs]=findpeaks(outputMag,f,'MinPeakHeight',thresholdOutput);

% now i also want to load frequency and complex gain into the peak finder

[pks,inputPeakIndex]=findpeaks(inputMag,'MinPeakHeight',thresholdInput);
clear pks

% this next part extracts the peaks at the various frequencies according to ONLY the input peaks
inputPeaks=elementSearch(input,inputPeakIndex);
outputPeaks=elementSearch(output,inputPeakIndex);
peakFrequencies=elementSearch(f,inputPeakIndex);

complexGain=outputPeaks./inputPeaks;

peakData.peakFrequenciesCSV=peakFrequencies;
peakData.complexGainCSV=complexGain;
peakData.samplingInterval_s = 1.0/max(peakFrequencies);


%% fourth, print the results
disp('use clf to clear the figures')
findpeaks(inputMag,f,'MinPeakHeight',thresholdInput);
hold on
findpeaks(outputMag,f,'MinPeakHeight',thresholdOutput);
disp('remember to also set hold off')
hold off
end



function [gainAndPhase] = gainAndPhase(freqData)
%% Summary of this function goes here

% this function just extracts the gain and phase of the inputs and outputs
% in freqData
% this takes the absolute values of output at each frequency and divides
% by the absolute values of input at each frequency

disp('warning, this may only work on LINEAR frequency response')

%   Detailed explanation goes here


% just make sure that  the frequency data is in columns. First column is
% frequency in Hz (not crucial), second is normalised FFT for input signal, third
% column is normalised FFT for output signal
%% first we get our frequency and etc first


f=freqData(:,1);
input=freqData(:,2);
output=freqData(:,3);

%% second gain matrix

gain=abs(output)./abs(input);

%% third let's get the phase for both

phaseInput=angle(input);
phaseOutput=angle(output);
phaseDiff=angle(output)-angle(input);

%% fourth reconstruct the matrix to give it out
% in the format, frequency, gain and phase

gainAndPhase=[f,gain,phaseDiff];

disp('%%%% gain and phase function complete')
end

