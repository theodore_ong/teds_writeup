%% so this file is supposed to take in transfer function for heater 
% power to temperature and plot both its bode plot and step response plot


a_kelvin_per_joule_second = 0.000129; 
b_hertz = 0.52;
c_hertz_squared = 0.0415;

numerator = [a_kelvin_per_joule_second];
denominator = [1,b_hertz,c_hertz_squared];

% ensure transfer fn is in Hz
sys = tf(numerator, denominator);

%% bode plot
options = bodeoptions;
options.FreqUnits = 'Hz';
[mag,phase,omega_out] = bode(sys);
bode(sys,options)

%convert to hertz 
frequency_out = 0.5/pi *omega_out;

% change dimensionality to fit (from AI generated search for syntax)
mag = reshape(mag,[1,61]);
mag = transpose(mag);
mag = 20*log10(mag);


phase = reshape(phase,[1,61]);
phase = transpose(phase);
% I'm exporting to csv so it's easy to plot in LaTeX
plot_data = [frequency_out,mag,phase];

% convert to table
plot_table = array2table(plot_data);
plot_table.Properties.VariableNames = {'frequency_Hz' , 'mag_dB', 'phase'};

writetable(plot_table, 'bode_one_dimension_heater_analytical.csv');


% respOpt = RespConfig(Amplitude=100);
% [stepfn_outlet_temp_100K,stepfn_time_100K]=step(transferFunctionSys_30K,respOpt);
% stepfn_outlet_temp_100K = 969.8 + stepfn_outlet_temp_100K;
% data_100K = [stepfn_time_100K,stepfn_outlet_temp_100K];
% data_header =["time_seconds","outlet_temp_kelvin"];
% data_100K = [data_header;data_100K];
% writematrix(data_100K,'fitted_transfer_fn_100K_step.csv');
% 
% respOpt = RespConfig(Amplitude=30);
% [stepfn_outlet_temp_30K,stepfn_time_30K]=step(transferFunctionSys_30K,respOpt);
% stepfn_outlet_temp_30K = 969.8 + stepfn_outlet_temp_30K;
% data_30K = [stepfn_time_30K,stepfn_outlet_temp_30K];
% data_header =["time_seconds","outlet_temp_kelvin"];
% data_30K = [data_header;data_30K];
% writematrix(data_30K,'fitted_transfer_fn_30K_step.csv');
% 
% respOpt = RespConfig(Amplitude=10);
% [stepfn_outlet_temp_10K,stepfn_time_10K]=step(transferFunctionSys_30K,respOpt);
% stepfn_outlet_temp_10K = 969.8 + stepfn_outlet_temp_10K;
% data_10K = [stepfn_time_10K,stepfn_outlet_temp_10K];
% data_header =["time_seconds","outlet_temp_kelvin"];
% data_10K = [data_header;data_10K];
% writematrix(data_10K,'fitted_transfer_fn_10K_step.csv');
%% this function is meant to take in the csv file of the frequency response 


freq_response_csv_to_bode_csv('one_dimension_ciet_cht_autotimestep_test', ...
    'bode_one_dimension_heater_library_simulated');

freq_response_csv_to_bode_csv('one_dimension_ciet_cht', ...
    'bode_one_dimension_heater_library_simulated_0_1_s_timestep');
%% List of Functions

function [fr_data]=peakData2idfrd(peakData)
%% this function takes multiple bode plots and creates a idfrd object which can be used to estimate
% a transfer function

%% one would obtain several plots of data first

frequencySize = size(peakData.peakFrequenciesCSV);
frequencySize = frequencySize(1)/2;

freq=peakData.peakFrequenciesCSV(1:frequencySize);
response=peakData.complexGainCSV(1:frequencySize);




%% now i load in my sampling interval (estimated)

Ts = peakData.samplingInterval_s;


% note that sampling time here is just to help eliminate any data point
% with frequency above nyquist frequency


% lastly create the idfrd object
% https://www.mathworks.com/help/ident/ref/idfrd.html#f4-1777348

fr_data=idfrd(response,freq,Ts,'FrequencyUnit','Hz');
%fr_data=idfrd(response,freq,Ts);

end

function [y,t] = test_transfer_fn(transfer_function_sys,amplitude)
% first some config for the transfer function
Config = RespConfig('InputOffset',0,'Amplitude',amplitude,'Delay',0);
[y,t] = step(transfer_function_sys,Config);

end

function [peakData] = linearPeakFinderPlotOverlay(freqData)
% basically i'm finding the peaks with their associated frequencies in this
% function, i output an object with both input and output peaks

%% first
% let's import the data from freqData to make it more readable
f=freqData(:,1);
input=freqData(:,2);
output=freqData(:,3);

%% second find the magnitude of the peaks

inputMag=abs(input);
outputMag=abs(output);

% decibels here will be used as the threshold for the peak prominence

thresholdInput=max(abs(inputMag))/20;
thresholdOutput=max(abs(outputMag))/20; % only input peaks are used 



%% third let's calculate our input and output peaks and frequencies
% https://www.mathworks.com/help/signal/ref/findpeaks.html

% load them straightaway into the data structure

[peakData.inputPeaksMag,peakData.inputPeakFreqs]=findpeaks(inputMag,f,'MinPeakHeight',thresholdInput);

[peakData.outputPeaksMag,peakData.outputPeakFreqs]=findpeaks(outputMag,f,'MinPeakHeight',thresholdOutput);

% now i also want to load frequency and complex gain into the peak finder

[pks,inputPeakIndex]=findpeaks(inputMag,'MinPeakHeight',thresholdInput);
clear pks

% this next part extracts the peaks at the various frequencies according to ONLY the input peaks
inputPeaks=elementSearch(input,inputPeakIndex);
outputPeaks=elementSearch(output,inputPeakIndex);
peakFrequencies=elementSearch(f,inputPeakIndex);

complexGain=outputPeaks./inputPeaks;

peakData.peakFrequenciesCSV=peakFrequencies;
peakData.complexGainCSV=complexGain;


%% fourth, print the results
disp('use clf to clear the figures')
findpeaks(inputMag,f,'MinPeakHeight',thresholdInput);
hold on
findpeaks(outputMag,f,'MinPeakHeight',thresholdOutput);
disp('remember to also set hold off')
hold off
end

%% the element search function used to isolate peaks based on location is here
% it takes in two vectors, 

% first vector is a vector of FFT data against some index
% second vector is a vector of indices
% the output vector just gives a vector of indices at that location

function [filteredFFTData]=elementSearch(FFTData,inputPeakIndices)

n=length(inputPeakIndices);
filteredFFTData=zeros(n,1);


for i=1:n
    peakIndex=inputPeakIndices(i,1);
    filteredFFTData(i,1)=FFTData(peakIndex,1);
end

end

%% function for exporting bode plots as png

% this will take a filename as string, current figure handle (use gcf to
% get current figure)
% and plot title as string

function export_bode_plot_as_png(bodeplot_handle,file_name,plot_title)

file_type = 'png';

% now resize the plots
bode_options = getoptions(bodeplot_handle);
bode_options.grid = 'on';
bode_options.Title.String = plot_title;
setoptions(bodeplot_handle,bode_options);


current_figure = gcf;

% resize the plot

x0=700;
y0=280;
width=800;
height=600;
set(current_figure,'position',[x0,y0,width,height]);


% export plot

saveas(current_figure,file_name,file_type);
end
