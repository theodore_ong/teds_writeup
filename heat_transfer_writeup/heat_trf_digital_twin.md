# Heat Transfer Digital Twin

## Introduction

From previous work, I already got a hydrodynamic solver for
parallel branches. This is an isotheraml digital twin.

### Tasks

1. Add a heat transfer solver
2. Validate natural convection cases
3. Validate Frequency Response 
4. Bring simulation within 80 ms using any means possible.


## Lit Review


### Nusselt Number Correlations

### Natural, Forced and Mixed Convection Algorithms

Personal notes:

I looked at RELAP manual, couldn't find the relevant equations:

https://inis.iaea.org/collection/NCLCollectionStore/_Public/28/018/28018923.pdf

https://www.nrc.gov/docs/ML1103/ML110330252.pdf

Also looked at SAM, didn't quite have equations either

https://publications.anl.gov/anlpubs/2019/08/154321.pdf

There was one i stumbled across, a porous debris heat transfer model,
probably not relevant

https://inldigitallibrary.inl.gov/sites/sti/sti/3156856.pdf

At the most basic, one can do openfoam, but that's way too slow
That's conjugate heat transfer on CFD level

https://develop.openfoam.com/Development/openfoam


And then here's another relap document, i searched, without
equations or etc.

https://inldigitallibrary.inl.gov/sites/sti/sti/3991951.pdf

Tried SAM manual, but also no equations displayed here
 for Nusselt Number

https://publications.anl.gov/anlpubs/2019/08/154852.pdf


Dane's Dissertation however, has the best data on what correlations
were being used...

page 117 and so on.

His dissertation title (draft)

A Frequency Domain Approach to Characterizing and Modeling
Single Phased, Forced Circulation Advanced Nuclear Reactor Design


Can't find on google scholar except as a citation


de Wet, D. (2020). A Frequency Domain Approach to Characterizing and Modeling Single Phased, Forced Circulation Advanced Nuclear Reactor Designs. University of California, Berkeley.



This is dane's other work
https://www.osti.gov/servlets/purl/1570910


But basically from UC Berkeley test bay, for CIET heater V1.0

Nu = 8 if Re $<$ 2300

Nu = 5.44 + 0.034 $* Re^{0.82}$

[110]	J. E. Bickel, N. Zweibaum, and P. F. Peterson, “Design 
Fabrication and Startup Testing in the Compact Integral Effects 
Test Facility in Support of Fluoride-Salt-Cooled, High-Temperature 
Reactor Technology,” UC Berkeley, Berkeley, CA, UCBTH-14-009, 
2014. [Online]. Available: http://fhr.nuc.berkeley.edu/wp-content/uploads/2015/04/14-009_CIET-IRP-Final-Report.pdf.

But after completion, in complete CIET facility,

$$Nu = (2.65*10^{-9})Re^3 - (1.82*10^{-5})Re^2 + 
(4.18*10^{-2})Re  - 22.1 $$

$$\Delta P  = (3.50*10^{-4}) Re^{-2} -1.13 Re + 1550$$

For heater v2.0, note that the above equations use
$D_{heated}$ rather than $D_H$

$$D_{heated} = 0.0381$$
$$Nu = 0.0391 * Re^{0.812} *Pr^{0.408} $$

If using hydraulic diameter, 

$$D_H = 0.01467m$$

$$Nu = 0.04179*Re^{0.836}*Pr^{1/3}$$

#### 19 Oct 2022


Correlations from Perry's

Green, D. W., & Southard, M. Z. (2019). Perry's chemical engineers' handbook. McGraw-Hill Education.


From Perry








### Quasi Steady State Heat Transfer, and Numerical Instability due to Large Time Step, Small Heat Capacity



## Methods

## Results

## Conclusion