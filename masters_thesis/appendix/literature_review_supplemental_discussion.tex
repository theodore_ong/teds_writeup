\subsection{Justification for Type I, Type II and Type III Digital Twin}

In the literature review, I provide some justification as to why 
I used a new ``Type I'', ``Type II'' and ``Type III'' naming
convention rather than use what was already available in literature.
In this part of the appendix, I provide further justification
as to why I use this particular naming convention.


\subsubsection{Why I include Plant Simulators as a Digital Twin}

One justification of using the proposed
naming convention could come
from how the word twin is used colloquially.
For digital twins, the language of the word twin brings about the 
idea of making a copy or describing two individuals who look 
almost identical to each other regardless of whether they keep
in contact for the rest of their lives. While we would also consider
that twins do not need to look identical, we would normally distinguish
such twins by adding the qualifier ``fraternal'' or the technical 
term ``dizygotic'' to it. \citep{Lupton2021} 
When we use the word twin without any
qualifier, the more common implied assumption is that the twins
are identical. Also the twins do not need to stay in contact with
each other throughout their lives to be considered twins.

I would suggest that likewise, digital twins should not need
to have one-way or two-way data exchange with the physical asset they
attempt to emulate to be considered a twin. I would simply say
they are Type I digital twins. So, real-time system models used
in simulators for operator training can be considered Type I
digital twins.
I would then go by the definition given by Boschert and Rosen
\citep{Boschert2016}. In this definition, a digital twin
is used to predict the behaviour of a real system in both
acquisition phase (includes design) and operation 
phase \citep{Boschert2016}.
Such intuition is also used in the same way in literature;
Several authors would agree they do not define digital twins
such that digital twins are digital twins if and
only if they share a two way data connection with the physical
asset that they are meant to replicate
\citep{Chen2017a, Rasheed2020,Eckhart2018}. 


\subsubsection{Why I do not define a Digital Twin by when it 
is used in the Product Life Cycle}

Finally, I would like to provide further justification for
using such a naming convention
as opposed to using other naming conventions. As mentioned earlier,
the convention of narrowing down the definition of a digital twin
such that a digital twin must have real-time one-way or two-way
data exchange is more popular in literature than the convention
that does not necessitate this data exchange. 
\citep{VanderValk2020, Kochunas2021}.
However, this naming convention
would be largely incompatible with other works in literature which
do not conform to this definition. Therefore, I opt not to use 
this narrow definition but rather have digital twins categorised into
Type I, Type II and Type III.\@ This is not the only typing
system used in literature, however. 

One example is the use
of the digital twin types $\alpha$ and $\beta$ to denote when
the digital twin is used in the life cycle of the technological
process \citep{Kholopov2019}. In this naming convention,
an $\alpha$ digital twin is used to describe
the technological process before it is implemented to manufacture
a specific product
while a $\beta$ digital twin is used for monitoring the
technological process after development is complete 
\citep{Kholopov2019}. In this regard, the naming convention is tied
more closely to the life cycle stage of the process or product
rather than the end goal of the digital twin. For the end user of 
the digital twin, however, the specific use case of the digital
twin may be of more immediate interest than the life cycle. At the 
end of the day, the user of the digital twin is seeking a specific
benefit provided by the digital twin and considering the cost
required to implement such a digital twin. In this regard, 
information about the life cycle of the digital twin may be of
less immediate use to the user. In digital twin taxonomy then,
we could develop a naming convention based solely upon the 
end use case of the digital twin. However, the use cases are so 
varied that it would be cumbersome for the user to get acquainted
with all the terms. The middle ground is then to grade digital twins
by their capabilities and relative cost of development. Capabilities
and cost of development is tied closest to the degree of synchronisation
required for digital twins. Therefore a typing system that is based
on the digital twin's degree of synchronisation with the physical
asset would make sense.

A summary of the types of digital twins and how they relate
to other similar terms in literature is provided in Table
\ref{digital_twin_types}. It should be noted that the $\alpha$
digital twin here is closest conceptually to the Type I digital
twin since both are used in the development phase where debugging and
implementation of the process is carried out. Whereas $\beta$ digital
twins are perhaps closely related to Type II digital twin since 
data about the technological process is sent to the operator in 
real-time \citep{Kholopov2019}. The operator is then free to decide
the actions to be performed thereafter. 

\subsubsection{Model Predictive Control vs Fault Tolerant 
Control}\label{appendix:MPC_vs_fault_tolerant_control}

To readers familiar with process control and controller design,
the use of digital twins to design fault tolerant controllers
bears much resemblance to model based predictive control 
\citep{Jones2020}.
Therefore, there is a need to distinguish them to provide 
the reader with some degree of clarity.

Model Predictive Control (MPC) is the process of using a model
to inform how controllers should behave so that the process can
carry on in a stable, safe and profitable manner \citep{Jones2020}.
For example, internal model control (IMC)
is a well established method of designing controllers for 
the petrochemical industry \citep{Garcia1989}. IMC takes advantage
of a mathematical model of the process, such as a transfer
function or state space model, and uses that to inform 
controller design. This method of using explicit 
process models to develop controllers is known as model
predictive control (MPC). IMC is just one method in the MPC
family \citep{Garcia1989}. And at a basic level, most IMC
controllers are based on linear transfer function models
of the process. 
In contrast to IMC, MPC allows us to use any form of 
mathematical model to help inform controller design. Since
digital twins are a subset of mathematical process models,
they can also be used in MPC controller design.

However, MPC methods are 
not necessarily fault tolerant. Basic Internal Model Control (IMC)
based controllers for instance, do not necessarily account
for system faults. Transfer functions used in IMC might reflect
how the system behaves in the normal manner. However, during
system faults, the appropriate transfer function needed to 
describe the system might change. If this model is not updated
to reflect the system fault, the IMC based controller may 
not be able to bring the system to a stable and safe
state. Such a system is not fault tolerant. However, if 
the transfer function and IMC controller 
is updated in real-time to reflect
system faults, we could perhaps consider such a control
system fault tolerant.

In the same way, the use of digital twins in MPC methods 
may not necessarily guarantee fault tolerance. These
Type III Digital Twins have to be synchronised 
in real-time to 
the physical system in order to be used for this use
case. 

\paragraph{Distinguishing Type II and Type III Digital Twins}\label{typeII_vs_typeIII}

In the previous example of describing a $\beta$ digital twin, which I
denoted as a Type II digital twin, real-time data is sent from the
plant to the digital twin so that the digital twin can use some internal
model to estimate information about the process or plant that
sensors are unable to measure. For convenience, I will use the term
``beyond-sensor-data'' to denote information about the state of the 
plant or process that cannot be directly measured by sensors and must
instead be estimated with the use of models. 

In a Type II digital twin setup, measured data is sent to the digital
twin in real-time, and beyond-sensor-data is generated in real-time
so that this information can be sent to the operator. The operator
will then decide the next course of action as he or she sees fit.
In this regard, the operator only periodically updates the state
of the plant as he or she sees fit because operators are human. 
It is reasonable for humans to have lapses in attention and therefore,
they cannot update the state of the physical plant using data
from the digital twin in real-time.

In contrast, a Type III digital twin setup would require real-time
exchange between the digital twin and the physical asset (plant or
process). This means that the digital twin sends beyond-sensor-data
to plant controllers directly in real-time, thereby influencing the
physical state of the plant in real-time. In this case, the Type III
digital twin setup completely bypasses the operator when it needs
to alter the state of the plant in real-time. 

\paragraph{Distinguishing a Type I Digital Twin from a Real-Time 
Simulation}

Another possible point of confusion is how a Type I digital twin
differs from a Real-Time Simulation.

We consider that digital twins are defined in literature as
digital representations
of a physical asset which tracks the characteristics, properties,
conditions and behaviours by means of models, information and data
\citep{Sleiti2022}. While a Type I digital twin does not track the
condition or state of a physical asset in real-time, it must at least
be able to replicate the behaviour or characteristics
of a plant in real-time. One key characteristic of a plant is that
it can be observed and controlled by human operators in real-time.
This capability, which
could be thought of as a key characteristic or behaviour
of the physical asset, should be
properly replicated or copied in a digital twin.

Of course, a digital twin must also be able to simulate the process
of the plant in real-time.
While a real time simulation could also perform real-time process
simulation
, I would distinguish digital twins from real time simulations
in that a human user must be able to have a two way data exchange
in order to interact with the digital twin in real time. This is 
because the physical asset has a capability for human users 
to interact with it in real time. 

\subsection{Digital Twin Synchronisation}\label{appendix:synchronisation}

\subsubsection{Clarification on what output may mean in State Output Synchronisation} 
When I mean output I do not only mean
desired process output, but also any sensor data which may
depend on a process input. To illustrate this, take for example
a system with a pump driving fluid through a water heater. 
The user may give an input, e.g a set point for water temperature
exiting the heater. The term output as used in control systems
terminology would be the actual temperature of the water exiting
the heater. Hence, we would have installed a temperature sensor
for the water heater outlet to measure the water heater outlet 
temperature.
This is an output we desire to control. However, in this
thesis, I would also consider intermediate temperatures such as 
the sensor measured
surface temperature of the water heater to be an output since
it depends on the set point. It may not be an output of interest,
and could be termed as an intermediate process variable,
but from the aspect of simulation and modelling, it is an output.
This is because in the context of digital twins,
we devise models that predict the surface 
temperature of the water heater, and that is in fact a model output
or an output of some function or program.
Of course, we would more likely spend our efforts
modelling an intermediate process variable such as the bulk
temperature within the water heater tank. Regardless, we would
also consider the bulk temperature within the water heater
to be an important modelling output even though it is an
intermediate process variable.
Therefore when I refer to outputs in the context of digital twins,
it will encompass these intermediate process variables as well.


\subsubsection{Typical 
Fault Detection Setups in Literature}\label{appendix:fault_detection_setups}

In a fault detection setup, a digital twin would emulate a physical
asset operating under normal conditions (otherwise known as a healthy
or fault free condition). When the behaviour of the physical system
deviated too much from the digital twin, an alarm would be triggered 
alerting the user that a fault was present. 

Now of course, the other reason that the physical asset can deviate
from the type II digital twin in terms of behaviour is that
the type II digital twin contains models with modelling errors.
This can result in modelling errors causing a misdiagnosis of 
system faults. To prevent this from happening, we may need to 
periodically synchronise or calibrate the models within the
digital twin with the physical system. Assuming this is done
correctly, we can now use the deviations between the type II 
digital twin and the physical asset to
alert users to faults which are present in the physical system.

In one example of such a case \citep{Jain2019}, 
photovoltaic cells were subject to a solar irradiance
and cell temperature over a period of time. A digital twin
of this experimental setup was constructed and has Its
inputs, cell temperature and solar irradiance,
synced to that of the physical solar cell. This digital
twin was assumed to be the fault free model of the 
photovoltaic cells.

The outputs of
the photovoltaic cells (e.g. voltage and current)
were subtracted from the estimated outputs of the digital
twin to produce a residual vector.  
The elements of this
residual vector were normalised using a scaling factor to 
ensure that the elements were dimensionless and were of
the same order of magnitude \citep{Jain2019}. Afterward,
the $L^\infty$ norm was taken of this vector and compared
against a threshold denoted as $\Gamma$ in the study
\citep{Jain2019}. $\Gamma$ was determined by measuring
the background noise of a fault free system, measurement
uncertainties, discretisation error and then adding
a margin of safety \citep{Jain2019}. 

Now, fault detection is merely the first step in getting the user
to the end goal of addressing the fault. By fault detection, I mean
that the presence of the fault is detected. However, the type of
fault plaguing the system still needs to be determined via 
fault identification.

To begin fault identification, what was done in literature was that
a series of faults were simulated in the digital twin \citep{Jain2019,
Dey2017} prior to startup of the physical asset. Each fault would produce
a specific behaviour or fault signature. These fault signatures would
then be collated into a fault signature library \citep{Jain2019}.
If the behaviour of the system matches one of the signatures within
the fault signature library, the nature or cause of the fault could 
then be identified. This was done in the case of the solar cell study
as well \citep{Jain2019}.

\subsubsection{Structural Analysis in Fault 
Detection}\label{appendix:structural_analysis}

Of course, such a simple approach for fault detection is presented
to help the reader understand why properly calibrated models 
are required for type II digital twins to detect faults. In literature,
more sophisticated methods for fault detection are presented
\citep{Poresky2019}. One example described in Poresky's work is 
the use of structural analysis \citep{Poresky2019} for fault
detection. 
Structural analysis provides
us with a systematic framework for identifying and isolating
faults in complex systems with several modelling equations and
several variables in the system.
Structural analysis methods are 
extensively discussed in literature for fault
detection
\citep{IzadiZamanabadi2002} and fault tolerant control
\citep{IzadiZamanabadi2000}.  
In fact,
a fault diagnosis toolbox written for Matlab and Python has
been written using this structural analysis method 
\citep{Frisk2017}. This shows that structural analysis
for fault detection is quite well established in literature.


In structural analysis, we also aim to diagnose faulty behaviour
and determine the cause of faulty behaviour. Poresky uses the 
term fault detectability to describe our ability to identify faulty
behaviour caused by a specific fault, and he uses the term fault
isolability to describe if a detected fault can be distinguished
from any other fault \citep{Poresky2019}.

For fault detection to occur, structural analysis dictates that
we need an over-determined system. Let me illustrate with an example:

Suppose we are trying to conduct a food and beverage business.
We are to receive crates of food from our supplier every month
These crates of food filled
to the brim would
weigh a set 50 kg if nothing goes wrong. However, if the crate
weighs heavier than 50 kg, one could get suspicious of our
suppliers. Of course, a truck driver delivering
the crate would say it weighs 50 kg. One way then to determine
how much the crate weighs is to believe what the truck
driver says.

In this example, we can reduce this scenario to an equation:

\begin{equation}
	m = 50~kg
\end{equation}

The mass of the crate, m is claimed by the driver to be 
50 kg. We have one equation as
described above, and one unknown. The equation simply
describes what the truck driver claims the crate weighs.
This system is just
determined as the number of equations is equal to the
number of unknowns. In such a system, there is no way
to tell if the truck driver is lying. Of course, in real life,
the truck driver could be ignorant or mistaken about 
the state of the crate, but for the sake of illustration, we 
assume that these other explanations do not apply.

We would need to cross-examine his claims with our own
way of measuring the mass of the crate. Suppose we have
our own weighing scale now, and it measures a mass 
measurement y. The variable y is of course known.

\begin{eqnarray}
	m = 50~kg \\
	m = y~kg
\end{eqnarray}

Supposing that y is known, and the uncertainty of y is negligible,
we now have two ways of determining the mass of the crate.
We have one unknown, m, and two equations to find the unknown.
Such a system of equations is over-determined since the number
of equations exceeds the number of unknowns. We can extend
this logic to complex systems with many variables and equations.

If the number of equations (i.e ways of measuring variables) 
exceeds the number of variables, then we have an over-determined
system and we are able to detect faults \citep{Poresky2019}.
However, identifying or isolating what fault is causing the errant
behaviour is another matter altogether.

In our example, if $y \neq  50$, then we have a fault. Either
the truck driver is lying or we have a faulty weighing scale.
In this case, we have no way of telling what issue has arisen
unless we assume the scale is accurate. In this case, the fault
is detectable but not isolable. To isolate the fault, we need
a way of determining if our weighing scale is working.

Suppose then we have an alternative way of checking if our scale
is working properly using a reasonably large spring of known stiffness
$k_{spring}$. We could attach a known test weight $m_{test}$ 
of perhaps 55 kg
to the spring and cause
it to oscillate on some sort of low friction environment to measure
the oscillation period T.

The additional equation describing this would be:

\begin{equation}
	T = 2\pi \sqrt{\frac{k_{spring}}{m_{test}}}
\end{equation}

Supposing that our spring is well calibrated and experiment
is conducted properly such that this equation applies, this
becomes a way for us to measure the test weight $m_{test}$. 
We can then use our weighing scale to measure the test weight
also. The measurement is denoted $y_{test}$.
Our system of equations becomes:

\begin{eqnarray}
	m = 50~kg \\
	m = y~kg \\
	m_{test} = y_{test} \\
	T = 2\pi \sqrt{\frac{k_{spring}}{m_{test}}}
\end{eqnarray}

Supposing that the test mass measurement from the spring experiment
matches $y_{test}$, then we have confidence that the problem is not
with our equipment and can infer that the truck driver is lying. 
However,
if the test mass measurement does not match $y_{test}$, we would
likely know that our equipment is surely at fault, not necessarily 
the truck driver.

By using a known test mass, we have effectively found a way to 
check if our scales were working properly independent of the truck
driver. If however, we did not use the test mass, and instead used
the crate with our spring system, our system of equations becomes:

\begin{eqnarray}
	m = 50~kg \\
	m = y~kg \\
	T = 2\pi \sqrt{\frac{k_{spring}}{m}}
\end{eqnarray}

If the y matches the mass measurement of the spring system, then
we would have confidence that our truck driver was lying. However,
if y did not match the measurement of the crate's mass by our
spring system, we would still have no way to tell if the measurement
discrepancy was due to the truck driver was lying or a faulty scale, or
if both cases were happening simultaneously. 
The only way to know for sure whether or not our scale is faulty
is to calibrate our scale with a test mass
regularly before using our weighing scale for checks.
Therefore, we need that test mass to
check our existing weighing scale.

This simple illustration helps us understand that for a fault to be
isolable, the set of equations that helps us describe a specific fault
must exclude all other model faults \citep{Poresky2019}. For this to
happen in this specific case, we wanted a system of equations
which could check if our weighing scale was faulty independent of
all other faults. Therefore,
we needed a test mass $m_{test}$ to ensure that the system
of equations describing the weighing scale calibration does not 
include the crate mass m. 

Now, whether models are used for fault detection or fault isolation,
we know we must have reasonably accurate and well calibrated models.
Since those models are found in digital twins, we must ensure that
the digital twin itself must be well calibrated. Secondly, for fault
isolation, we must still accurately model how each fault impacts our system
of equations. Only then can we properly distinguish one fault from
another by devising alternative means of measurement. 
A more complete and formal explanation of this process can be found
in literature \citep{Poresky2019, IzadiZamanabadi2002}. 

In this master's
thesis, I do not aim to explain structural analysis methodology for
fault detection. 
However, I only want to elaborate enough to show
that accurate modelling of the system is required to detect faults
and accurate modelling of faults is required in order to isolate
faults. 

\subsubsection{Synchronisation 
Requirements for Various Applications}\label{appendix:synchronisation_requirements}

\paragraph{Requirements for Fault Tolerant Control}

In such a setup, we could diagnose faults using Type II digital
twins which are synchronised to the physical system in real-time.
Information about these faults can be processed into a 
signal we could send to a controller to mitigate those faults
without the need for immediate user intervention. This has
been demonstrated in literature for simple manufacturing processes
\citep{Majdzik2022}. Sending such
signals to the physical system in real-time from the digital twin
by definition requires the digital twin to be a Type III digital twin.
This is because there is a two-way real-time data exchange between
the digital twin and the physical asset. 

However, making control decisions based on a digital twin would
put the plant at risk especially if those decisions were based 
on low accuracy or low fidelity data. Therefore, we would want 
our digital twin to provide data in as accurate and timely a 
manner as possible. This might sometimes mean that we would
want accurate estimates about process information 
beyond what sensors can provide. Therefore, we might not only
need to have any Type III digital twin, but a Type III digital
twin that is able to synchronise itself to the full state of
the plant in real-time. Using the terminology in 
section~\ref{stateSynchronisationTerminology}, we are performing
full state synchronisation in real-time.

\subparagraph{Prototyping Fault Tolerant Controllers}

In the operation of fault tolerant control, we might need a Type
III digital twin with real-time full state synchronisation.

However, when we are developing and testing fault tolerant
control designs, we might resort to using Type I digital twins.
In such a setup, digital twins have to be used to generate
fault signature libraries just as before \citep{Saraeian2022}.
This is still important information we need for developing
fault tolerant controllers. 

Moreover, once we have controller prototypes ready for testing,
we might use Type I digital twins to perform what-if analysis
to analyse how these controllers behave in various scenarios.
In literature, this approach was used to design controllers 
meant for the benchmark Tennessee Eastman (TE) chemical process
\citep{He2019}. 

We might not only perform prototyping of new controllers during
the beginning of plant life. As the plant ages, we may want 
to update these controllers as well. As such, the use of Type I
digital twins for prototyping fault tolerant controllers might
extend well beyond the initial design phase of the plant.


\paragraph{Monitoring Process Variables of Physical Asset}


Digital twins can be used in monitoring of the
process variables of a plant or physical asset in 
real-time. This goes beyond
fault detection discussed in section~\ref{faultDetection}.
This is because fault detection only requires that a plant's
behaviour deviates from the norm. In fault detection,
we only need to compare the sensor readings of the physical
asset against the estimated sensor readings of the digital
twin in order to detect a fault. This was done in the 
photovoltaic cell study \citep{Jain2019}. 
In contrast,
the real-time monitoring of the process variables 
of a plant or physical 
asset involves using a digital twin to estimate data about 
the physical asset in areas where sensors are impractical 
to install. This means we are estimating ``beyond sensor
data''.

In such a case,
the end user may use a mathematical model of a physical asset
in order to estimate data about the physical asset where said  
data is not reachable by sensors. For example, in the case
of batteries, temperature data within the electrolytes of the 
battery may not be available. Nevertheless, in literature,
they were estimated with the use of lumped parameter models 
\citep{Dey2016}
as well as partial differential equation (PDE) models 
\citep{Dey2017}. This allows the user to have an estimate of 
the core temperature of the battery or in the case of 
more sophisticated
PDE models, temperature distribution within the battery 
\citep{Dey2017}. In that study, having internal temperature
distribution estimates of the battery allowed for a more
sophisticated version of fault detection to occur 
\citep{Dey2017}. However, it should be noted that this 
is not strictly required for fault detection in general.
Nevertheless, having a digital twin with plant state monitoring
capabilities could enhance fault detection algorithms provided
the digital twin is kept synchronised with the physical asset.
Additionally, the fault signature library or database should
be kept up to date as well.

Now of course, a digital twin meant to estimate all the sensor
outputs for fault detection may likely already have all 
the data values necessary to monitor the entire state of the plant.
This is especially true in the case of digital twins based on
physical models or first principles. However,
A designer might opt to use a data driven model because the first
principles model might be too slow for real-time calculations and
become a bottleneck in digital twin development \citep{Rasheed2020}.
As a result, we might have the digital 
twin were based on a purely
data driven model.
If the designer had not included the other sensor
unreachable process variables in the data driven
model, then these models would
not likely have the predictive power present in first principle
type models.
In such a case, the fault detection function can be performed
without knowing the full internal state of the plant.

Therefore, we can show that monitoring the internal state of a plant,
including all its process variables,
is distinct from fault detection processes. You could also perform
monitoring of the plant without any fault detection algorithms in place.
These two functions are essential parts of online plant health monitoring,
but are distinct from each other.

\subparagraph{Requirements for Process Variable Monitoring of Physical Asset}

If we want to know or estimate the internal 
state of the physical asset (beyond-sensor-data) in real 
time, then input, output and model synchronisation has to 
be done in real-time as defined in~\ref{stateSynchronisationTerminology}.
This by definition necessitates a Type II digital twin at least.


\paragraph{Predictive Maintenance}

Now digital twins have been shown to have utility in fault detection 
and in developing fault tolerant controllers. In fact, the 
operation of fault tolerant controllers usually necessitates
a Type III digital twin with real-time full state synchronisation
capabilities. These digital twins should also have 
capability to provide
the user with information beyond what can be measured
directly with sensors (i.e beyond-sensor-data).

Such a digital twin would take considerable effort to develop. 
However, an additional reason to develop such a digital twin
is the potential to use such a digital twin in predictive maintenance
\citep{Rasheed2020, Fotland2020}.
Without digital twins, maintenance is usually done
periodically to prevent any kind of stress related failures. 
However, the frequency of maintenance can often be higher than
what is actually required, and performing unnecessary
maintenance can accelerate the aging of components. 
While this may be the case, periodic maintenance 
is done so that the physical
asset can be operated in a safe manner. However, if we want to 
reduce the frequency of maintenance in a safe manner, we would
need to understand how the physical asset undergoes wear and 
tear using some simulation model. 
When the estimated wear and tear exceeds some threshold,
we stop the operation of the physical asset and perform maintenance.
The overall effect of the latter approach is a higher 
degree of operation time and increased component life.

An example of this case is in cranes in offshore
vessels \citep{Fotland2020}.
Offshore cranes will experience stress and fatigue during
lifting operations. To detect if the crane is experiencing
unsafe levels of stress, we could install sensors within the
crane or in a physical twin of the crane.
However, similar to the case of batteries, sensors 
are limited in their
capabilities to measure stress along the entire structure of
the crane. Moreover, having physical twins of the cranes would
be quite costly to construct and operate.

The alternative then is to construct a digital twin of the
crane and predict the accumulated stresses in each part of
the crane so that the operators can better predict if or when
parts of the crane would fail. This digital twin can then
be used as a basis for risk analysis when planning for operations.
\citep{Fotland2020}

\subparagraph{Requirements for Predictive Maintenance}

In these examples, we take advantage of the digital twin's
ability to have an educated guess about the state of the physical
asset everywhere within the said physical asset, even 
for state parameters that cannot be measured directly with
sensors.

This means that we would like a digital twin to have real-time
full state synchronisation capabilities as described in 
section~\ref{stateSynchronisationTerminology}. 
This is because we may want to know the information about 
the physical assets beyond what sensors can tell us.
Furthermore, we may want to keep a history of the state of 
the system. This is because the state of the system presently
might depend of the history of the system state. For example,
the metal fatigue within some region of the offshore crane
will depend on the magnitude of stress experienced by the crane in 
the past as well as the duration of stress experienced.
This examples illustrates how fatigue is dependent on the history
of load cycles and how material stress and strain relationships
show hysteresis or memory dependence \citep{Erber1993}.
As such it is important for us to have a record of the past states
of the system over time in order to predict its present state.
Only when one has sufficiently accurate knowledge about the 
state of the physical system can one execute predictive maintenance
safely.

By virtue of these requirements, this digital twin
will at least have to be a Type II digital twin since the digital
twin receives data from the physical asset in real-time. We 
might not necessarily need a Type III digital twin depending
on the use case. For example, when users wish to check if a crane
or support has experienced a critical level of low cycle fatigue, 
the user may need only check the digital twin periodically (perhaps
every week or every month) rather than in real-time (every 0.3 seconds).
