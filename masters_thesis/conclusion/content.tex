This work shows that a prototype isothermal flow digital twin of CIET was 
constructed successfully using Rust and Python. This digital 
twin was validated
using isothermal flow experimental data available in literature
\citep{Zweibaum2015,Bickel2014}. The data used was isothermal flow data 
from flow in the CTAH and Heater branches, as well as flow data
in CTAH and DHX branches. The deviation from experimental data for both cases
is about $\pm 50 Pa$ or $\pm 2\%$ whichever is greater. This was well within
flowmeter error and manometer correlation error. 
The Type I Digital Twin could also estimate
mass flowrates through the three branches, CTAH for reverse flow 
through the DHX and Heater Branches.
For this setup, rigourous error estimation of the predicted data
was not performed due to the complexity of error estimation through
parallel branches. Instead, crude error bounds were given based on
10\% $\zeta$, manometer correlation error, and $\pm 2\%$ flowmeter error.
More detailed error analysis will be required in future 
if the goal is to simulate flow through parallel flow paths.
This work also shows that the parallel branch approximation approach for
CIET is workable because even when branch points were shifted slightly for Branch
5 and 17 of CIET, the loop pressure drop data was still able to be 
replicated within experimental and correlation uncertainty.
This allowed for simulation times to be reduced to an acceptable 
timeframe.

This digital twin could also 
be interacted with in real time via an OPC-UA server
and client interface, and it was was able
to run within 100 ms for both the lower end Ideapad 3 and the gaming laptop
if the only two out of three branches were included in calculations. Whereas
when the DHX branch was added with check valve behaviour, only the gaming
laptop managed to provide accurate results within 100 ms. This shows that
even with unoptimised Rust binaries, the digital twin could be run without the
need for high performance computer clusters.

It is also important that the libraries built here were constructed
using open source software. The source code is available in the 
Appendix~\ref{appendix:source_code}
for readers who are interested in replicating the results. 

\section{Future Work}

To complete the digital twin, one must move beyond isothermal flow 
into heat transfer. Similar to the SAM model \citep{Zou2019}, natural
convection flows should be validated and at a later time, transients
and frequency response test results from CIET should be used to 
validate the digital twin.

With each step however, the computational burden would increase with
each timestep. It is therefore desirable to introduce parallelisation
or optimised compilation
in future work to ensure that at each timestep, all the required calculations
can be completed within 100 ms or less. 

Should we successfully complete this venture, the next steps could be 
adapting the Type I digital twin into a Type II or Type III digital twin
with real-time state synchronisation capabilities. This could be a study
to use the digital twin as a tool for fault detection or fault tolerant
control as described in the literature review. We could even 
develop a prototype of a Type III digital twin for use in predictive
maintenance.

One other use of the Type I digital twin could be in a simulated neutronics
feedback study. Such a study requires development of a heater controller
which simulates coupled reactor physics and thermal hydraulics behaviours.
The Type I Digital Twin would then serve as a safe testing and development
environment for such a controller.
Should the controller be properly developed, I may want to investigate the impact
of a loss of heat sink (LOHS) incident on the pipe temperatures when 
the reactor is running at full power. 
Unfortunately, CIET is that it is scaled to about 2\% of the Mark I 
FHR at prototypical conditions \citep{Zweibaum2015}. This means its 10 kWth
heater is scaled to about 500 kWth of power or 0.5 MWth of heater power.
A typical demonstration reactor such as the Karios Power Hermes Reactor
would be around 35 MWth. To have CIET scale to that power, it would need
to be about 70 times more power. In other words, we would need to have
a 700 kWth heater at least or 0.7 MWth. This is not practical for
CIET due to the level of power draw required and potential safety concerns
of installing a heater that powerful.
Even if those issues were solved, the cost of running such a setup would
likely prohibit any extended operation of such an apparatus.

Thankfully, with type I digital twin of CIET, we may be able to simply 
increase heater power by changing one line of code. I may also be able
to artificially increase pump power to reach an appropriate mass flowrate,
and then perform a LOHS scenario study at this power level. In this
case however, we must then delve into predicting the uncertainty of the
extrapolated flow data.

As illustrated here, once the digital twin is constructed, there are many
possibilities for research and scenario assessment. Also, once the 
libraries and source code for the Type I Digital Twin
are completed, I intend to release them with an open source license 
so that
research and development in digital twins can be accelerated rather than
be bottlenecked by a paywall. If this is done well, this Digital Twin
codebase could be one of the first Type I
Digital Twins and perhaps the first Type I Digital Twin (and 
system level program) of CIET to be released under an 
open source license. This is intended
for making Digital Twin research more accessible.
