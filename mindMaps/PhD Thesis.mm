<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1629747046844" ID="ID_1015478396" MODIFIED="1629747068075" TEXT="PhD Thesis">
<node CREATED="1629747060636" ID="ID_11097952" MODIFIED="1629747080115" POSITION="right" TEXT="arbitrary reactor simulations">
<node CREATED="1629747159736" ID="ID_1056366734" MODIFIED="1629747164738" TEXT="MGXS Generation">
<node CREATED="1629747202118" ID="ID_1131393029" MODIFIED="1629747220130" TEXT="arbitrary reactor 1">
<node CREATED="1629747286382" ID="ID_797354480" MODIFIED="1629747289490" TEXT="crude reactor"/>
<node CREATED="1629747291214" ID="ID_1181083306" MODIFIED="1629747373938" TEXT="fast spectrum"/>
</node>
<node CREATED="1629747224086" ID="ID_869529493" MODIFIED="1629747230762" TEXT="arbitrary reactor 1.1">
<node CREATED="1629747232054" ID="ID_114413012" MODIFIED="1629747236074" TEXT="corrected some BCs"/>
<node CREATED="1629747239734" ID="ID_1447344844" MODIFIED="1629747245114" TEXT="still a fast reactor"/>
</node>
<node CREATED="1629747247846" ID="ID_1263500528" MODIFIED="1629747251426" TEXT="arbitrary reactor 2">
<node CREATED="1629747252686" ID="ID_979511003" MODIFIED="1629747257666" TEXT="includes control rods"/>
<node CREATED="1629747259582" ID="ID_711626158" MODIFIED="1629747267625" TEXT="may include triso">
<node CREATED="1629747376837" ID="ID_903561567" MODIFIED="1629747382458" TEXT="Tasks">
<node CREATED="1629747383718" ID="ID_1361264283" MODIFIED="1629747405506" TEXT="learn to create triso particles in OpenMC"/>
<node CREATED="1629747414750" ID="ID_1249768831" MODIFIED="1629747420000" TEXT="get model to criticality"/>
</node>
<node CREATED="1629747440781" ID="ID_1527033366" MODIFIED="1629747443537" TEXT="open questions">
<node CREATED="1629747444510" ID="ID_18901695" MODIFIED="1629747450473" TEXT="but what about parametric studies?">
<node CREATED="1629747454262" ID="ID_281631895" MODIFIED="1629747457601" TEXT="triso vs non triso"/>
<node CREATED="1629747459790" ID="ID_1467426083" MODIFIED="1629747467929" TEXT="multigroups vs two groups"/>
<node CREATED="1629747472126" ID="ID_965007707" MODIFIED="1629747478353" TEXT="spatial discretisaion?"/>
<node CREATED="1629747495798" ID="ID_1938205741" MODIFIED="1629747505977" TEXT="maybe i&apos;m trying to bite too much off..."/>
</node>
</node>
</node>
<node CREATED="1629747270534" ID="ID_1841815763" MODIFIED="1629747285231" TEXT="excludes">
<node CREATED="1629747392045" ID="ID_1983171714" MODIFIED="1629747394841" TEXT="Xenon"/>
</node>
</node>
</node>
<node CREATED="1629747171223" ID="ID_926085113" MODIFIED="1629747185722" TEXT="GeN-Foam Modelling"/>
</node>
<node CREATED="1629747083343" ID="ID_854845585" MODIFIED="1629747098074" POSITION="left" TEXT="experiments with CIET"/>
<node CREATED="1629747100751" ID="ID_943768425" MODIFIED="1629747110602" POSITION="right" TEXT="conversion to labview transfer functions">
<node CREATED="1629747525150" ID="ID_260043749" MODIFIED="1629747529801" TEXT="arb reactor 1 transfer fn">
<node CREATED="1629747534661" ID="ID_94476238" MODIFIED="1629747537592" TEXT="generated scripts"/>
<node CREATED="1629747539661" ID="ID_1599057208" MODIFIED="1629747546697" TEXT="transfer function available"/>
<node CREATED="1629747551430" ID="ID_1705112073" MODIFIED="1629747554017" TEXT="documented"/>
</node>
<node CREATED="1629747555862" ID="ID_798165880" MODIFIED="1629747561833" TEXT="arbitrary reactor v1.1 transfer fn">
<node CREATED="1629747563061" ID="ID_940055065" MODIFIED="1629747573265" TEXT="not documented"/>
<node CREATED="1629747574462" ID="ID_208742857" MODIFIED="1629747580481" TEXT="transfer fn not available yet"/>
</node>
</node>
</node>
</map>
