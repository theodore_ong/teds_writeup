#!/bin/bash 

echo "pdf_compress_gs: compresses pdf file"
echo "usage:"
echo "pdf_compress_gs [filename] [output_filename]"
pdf_compress_gs() {
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$2 $1
}
