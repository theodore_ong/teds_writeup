now this is the file for dissertation

to use it best, you will need to do this on linux,

or at least windows subsystem on linux

Have a texmaker ready with texlive-full

or at least
texlive
biber (biblatex)
texlive-extarrow
texlive-biblatex-apa
texlive-appendix

texmaker

and also the relevant git tools

libreoffice is needed for powerpoint slides


# Compilation 

To compile my PhD dissertation, it is difficult with limited memory due 
to many many tikz plots being used. 

To compile it, use this command:

```bash
latexmk -pvc -pdf -shell-escape --interaction=nonstopmode working_copy.tex
```

The working copy of my dissertation is called working_copy.tex, but the 
dissertation may be called Dissertation_Ted.tex.


